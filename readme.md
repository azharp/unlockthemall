
+=============================================+
|       UnLock Them All! (UTA) v3.7.3         |
| Copyright (c) 2007 Just Script Kidding Team |
+=============================================+


UnLock Them All! adalah program sederhana yang dibuat untuk 
membantu dalam memulihkan komputer yang terkena virus. Didalam
program ini terdapat 4 fungsi utama dan beberapa fungsi 
tambahan lainnya.


Fitur-fitur utama pada UnLock Them All! :
=========================================

1. Registry Manager
   ----------------
   Berguna untuk mengatur settingan umum yang biasa diubah melalui
   Windows Group Policy Editor (gpedit.msc). Terdapat juga fungsi
   untuk menyimpan hasil settingan ke file (UTASettings.ini).
   Untuk menyimpan settingan, klik kanan pada jendela Registry
   Manager dan pilih Save/Load Settings -> Save Settings.

2. Process Manager
   ---------------
   Berguna sebagai pengganti Windows Task Manager, yang memiliki
   kemampuan untuk mematikan banyak proses sekaligus. Saat berguna
   untuk virus yang memiliki banyak proses.

3. Startup Manager
   ---------------
   Berguna untuk mengatur program-program yang berjalan pada saat
   Startup. Anda dapat menghapus atau memindahkan setting startup
   program yang diinginkan.
   
4. Window Manager
   --------------
   Berguna untuk mengatur window dari program yang sedang berjalan.
   Anda dapat menampilkan/menyembunyikan window dari program yang
   sedang berjalan, mengubah transparansi dari window, mengubah
   status window secara paksa (maximize, minimize atau restore) dan
   mengubah status control (enable/disable, visible/invisible).


Fitur-fitur tambahan pada UnLock Them All!:
===========================================

1. Shutdown, Restart, Logoff, Stand by dan Hibernate melalui
   tray icon.

2. Fitur Quick Info, untuk menampilkan status secara cepat.

3. Fitur Disable Feature, untuk menonaktifkan : Tombol Start Menu,
   System Tray, Taskbar, Jam dan Quick launch pada windows.

4. Fitur Quick Access ketika program dalam keadaan minimized,
   dapat diakses dari tray icon.


File yang terdapat didalam paket :
==================================

1. UnLock Them All!.exe ---> Program utama UnLock Them All!.
2. UTASettings.ini ---> File untuk menyimpan settingan UnLock Them All!.
3. Readme.txt ---> File yang sedang anda baca sekarang.


Program History :
=================

Version 3.7.3
-------------

- [Registry Manager] Perubahan User Interface.
- [Window Manager] Penambahan opsi filter.
- Perbaikan beberapa bug.
- Penggantian icon program (lagi).
- Optimasi kode.

Version 3.7.2
-------------

- Penggantian icon program.
- Perbaikan bug pada fungsi Incremental Refresh.

Version 3.7.1
-------------

- [Registry Manager] Penambahan tooltip untuk setiap opsi.
- [Process Manager] Perbaikan fungsi Incremental Refresh.
- [Process Manager] Perubahan metode Terminate Process.
- [Window Manager] Perbaikan fungsi Incremental Refresh.
- [Window Manager] Penambahan fungsi Control State.
- [Tray Menu] Perbaikan bug pada Quick Access.

Version 3.7.0
-------------

- [Registry Manager] Penambahan setting Hide My Computer.
- [Registry Manager] Penambahan setting Hide Username.
- [Process Manager] Penambahan fungsi Terminate & Delete Process.
- [Process Manager] Penambahan fungsi Suspend/Resume Process.
- [Process Manager] Penambahan fungsi Restart Process.
- [Process Manager] Sedikit perubahan pada context menu.
- [Process Manager] Penambahan Incremental Refresh.
- [Window Manager] Penambahan Incremental Refresh.
- [Tray Menu] Penambahan fitur Quick Access.

Version 3.6.5
-------------

- Perbaikan beberapa bug.
- Optimasi kode.

Version 3.6.4
-------------

- Penggantian compressor program (FSG >>> UPX)
  karena terdeteksi sebagai virus oleh beberapa antivirus.

Version 3.6.3
-------------

- Perbaikan pada fungsi Quick Info.
- Sedikit perubahan pada antarmuka pengguna.
- Optimasi kode.


Info tambahan :
===============

Untuk versi terbaru & program lainnya silakan
download di : 

gutz.4shared.com
----------------

Untuk bug-report silakan kirim ke e-mail :

azharpratama@gmail.com
----------------------


Our motto:
==========

Just Script Kidding Team --- "We don't understand but it works!"


END OF FILE.....