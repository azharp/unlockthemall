VERSION 5.00
Begin VB.Form formLoad 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   465
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   2910
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H8000000F&
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   465
   ScaleMode       =   0  'User
   ScaleWidth      =   2910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Visible         =   0   'False
   Begin VB.Frame frmMain 
      Height          =   495
      Left            =   30
      TabIndex        =   0
      Top             =   -50
      Width           =   2865
      Begin VB.PictureBox picMain 
         BorderStyle     =   0  'None
         Height          =   245
         Left            =   100
         ScaleHeight     =   240
         ScaleWidth      =   2670
         TabIndex        =   1
         Top             =   175
         Width           =   2670
         Begin VB.Label lblWait 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Please wait..."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   20
            TabIndex        =   3
            Top             =   0
            Width           =   1095
         End
         Begin VB.Label lblStatus 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   1215
            TabIndex        =   2
            Top             =   0
            Width           =   45
         End
      End
   End
End
Attribute VB_Name = "formLoad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

    If bLoaded Then
        lblStatus.Caption = "Saving settings"
        bLoaded = False
        formLoad.Show , formMain
    Else
        lblStatus.Caption = "Loading settings"
        bLoaded = True
        formLoad.Show
    End If
    DoEvents

End Sub

