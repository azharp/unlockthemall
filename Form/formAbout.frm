VERSION 5.00
Begin VB.Form formAbout 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   3585
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   4395
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "formAbout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3585
   ScaleWidth      =   4395
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Visible         =   0   'False
   Begin VB.CheckBox chkUnload 
      Caption         =   "Unload"
      Enabled         =   0   'False
      Height          =   255
      Left            =   3480
      TabIndex        =   4
      Top             =   3720
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Timer tmrFade 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   600
      Top             =   3720
   End
   Begin VB.Timer tmrScroll 
      Interval        =   1
      Left            =   120
      Top             =   3720
   End
   Begin VB.PictureBox picOut 
      BackColor       =   &H00FFFFFF&
      Height          =   3585
      Left            =   0
      ScaleHeight     =   3525
      ScaleWidth      =   4335
      TabIndex        =   0
      Top             =   0
      Width           =   4395
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   -120
         ScaleHeight     =   735
         ScaleWidth      =   4695
         TabIndex        =   5
         Top             =   3200
         Width           =   4695
         Begin VB.Label lblUpdate 
            BackStyle       =   0  'Transparent
            Caption         =   "Check for Update"
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   3120
            TabIndex        =   19
            Top             =   90
            Width           =   1260
         End
      End
      Begin VB.PictureBox picUp 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   1005
         Left            =   -15
         ScaleHeight     =   1005
         ScaleWidth      =   4410
         TabIndex        =   1
         Top             =   -30
         Width           =   4410
         Begin VB.Line Line2 
            BorderColor     =   &H00000000&
            BorderWidth     =   2
            Index           =   6
            X1              =   112
            X2              =   4267
            Y1              =   900
            Y2              =   900
         End
         Begin VB.Label lblDesc 
            AutoSize        =   -1  'True
            BackColor       =   &H00000000&
            BackStyle       =   0  'Transparent
            Caption         =   "Simple Multi-Purpose Utilities"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000007&
            Height          =   195
            Left            =   945
            TabIndex        =   3
            Top             =   615
            Width           =   2490
         End
         Begin VB.Label lblMain 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "UnLock Them All!"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   15
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000007&
            Height          =   360
            Left            =   885
            TabIndex        =   2
            ToolTipText     =   "Don't double click me please!"
            Top             =   105
            Width           =   2625
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00000000&
            BorderWidth     =   2
            Index           =   5
            X1              =   112
            X2              =   4267
            Y1              =   570
            Y2              =   570
         End
      End
      Begin VB.PictureBox picIn 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   7995
         Left            =   120
         ScaleHeight     =   7995
         ScaleWidth      =   4110
         TabIndex        =   6
         Top             =   -4800
         Width           =   4110
         Begin VB.Label lblCont 
            Alignment       =   2  'Center
            BackColor       =   &H80000009&
            BackStyle       =   0  'Transparent
            Caption         =   "Copyrightę 2007 Just Script Kidding Team"
            ForeColor       =   &H00FF0000&
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   18
            Top             =   7680
            Width           =   3975
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00000000&
            Index           =   4
            X1              =   120
            X2              =   1785
            Y1              =   4800
            Y2              =   4800
         End
         Begin VB.Label lblCap 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "Disclaimer :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   4
            Left            =   180
            TabIndex        =   17
            Top             =   4560
            Width           =   1110
         End
         Begin VB.Label lblCont 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   $"formAbout.frx":000C
            ForeColor       =   &H00000000&
            Height          =   885
            Index           =   5
            Left            =   240
            TabIndex        =   16
            Top             =   4920
            Width           =   3615
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00000000&
            Index           =   3
            X1              =   120
            X2              =   1785
            Y1              =   3720
            Y2              =   3720
         End
         Begin VB.Label lblCont 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            ForeColor       =   &H00000000&
            Height          =   720
            Index           =   4
            Left            =   240
            TabIndex        =   15
            Top             =   3840
            Width           =   3615
         End
         Begin VB.Label lblCap 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "Information :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   3
            Left            =   180
            TabIndex        =   14
            Top             =   3480
            Width           =   1275
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00000000&
            Index           =   2
            X1              =   120
            X2              =   1785
            Y1              =   6120
            Y2              =   6120
         End
         Begin VB.Label lblCont 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   $"formAbout.frx":00CB
            ForeColor       =   &H00000000&
            Height          =   930
            Index           =   3
            Left            =   240
            TabIndex        =   13
            Top             =   6240
            Width           =   3615
         End
         Begin VB.Label lblCap 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "Thanks to :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   2
            Left            =   120
            TabIndex        =   12
            Top             =   5880
            Width           =   1080
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00000000&
            Index           =   1
            X1              =   120
            X2              =   1785
            Y1              =   1590
            Y2              =   1590
         End
         Begin VB.Label lblCont 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   $"formAbout.frx":0135
            ForeColor       =   &H00000000&
            Height          =   1665
            Index           =   2
            Left            =   240
            TabIndex        =   11
            Top             =   1755
            Width           =   3615
         End
         Begin VB.Label lblCap 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "Comments :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   1
            Left            =   180
            TabIndex        =   10
            Top             =   1320
            Width           =   1140
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00000000&
            Index           =   0
            X1              =   120
            X2              =   1785
            Y1              =   360
            Y2              =   360
         End
         Begin VB.Label lblCont 
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "Universitas Pendidikan Indonesia               Fakultas FPMIPA, Program Studi Ilmu Komputer"
            ForeColor       =   &H00000000&
            Height          =   480
            Index           =   1
            Left            =   240
            TabIndex        =   9
            Top             =   735
            Width           =   3615
         End
         Begin VB.Label lblCont 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "Azhar Pratama (azharpratama@gmail.com)"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   8
            Top             =   480
            Width           =   3705
         End
         Begin VB.Label lblCap 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "Developed by :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   180
            TabIndex        =   7
            Top             =   120
            Width           =   1455
         End
      End
   End
End
Attribute VB_Name = "formAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private CurScheme    As Long
Private EasterFlag   As Boolean
Private Nilai        As Long
Private Masuk        As Boolean

Private Sub ChangeState(ByVal State As Long)

Dim myC As Control

    Select Case State
    Case 1
        For Each myC In Me.Controls
            If TypeOf myC Is PictureBox Then
                myC.BackColor = vbBlack
            ElseIf TypeOf myC Is Label Then
                myC.ForeColor = vbGreen
            ElseIf TypeOf myC Is Line Then
                myC.BorderColor = vbRed
            End If
        Next myC
    Case 2
        For Each myC In Me.Controls
            If TypeOf myC Is PictureBox Then
                myC.BackColor = vbWhite
            ElseIf TypeOf myC Is Label Then
                myC.ForeColor = vbBlack
            ElseIf TypeOf myC Is Line Then
                myC.BorderColor = vbBlack
            End If
        Next myC
    Case 3
        For Each myC In Me.Controls
            If TypeOf myC Is PictureBox Then
                myC.BackColor = vbBlack
            ElseIf TypeOf myC Is Label Then
                myC.ForeColor = vbRed
            ElseIf TypeOf myC Is Line Then
                myC.BorderColor = vbGreen
            End If
        Next myC
    Case 4
        For Each myC In Me.Controls
            If TypeOf myC Is PictureBox Then
                myC.BackColor = vbBlack
            ElseIf TypeOf myC Is Label Then
                myC.ForeColor = vbWhite
            ElseIf TypeOf myC Is Line Then
                myC.BorderColor = vbWhite
            End If
        Next myC
    End Select

End Sub

Private Sub Form_Load()

    Nilai = 0
    Masuk = True
    SetLayered Me.hwnd, True, CByte(0)
    tmrFade.Enabled = True
    CurScheme = 2
    EasterFlag = False
    tmrScroll.Interval = 20
    picIn.Top = picOut.ScaleHeight + 20
    Me.Caption = "                          " & AppVar.AppTitle & " v" & AppVar.AppVersion
    lblCont(4).Caption = "Versi " & AppVar.AppVersion & ", Release tanggal : 3 Agustus 2007." & vbNewLine & _
                         "Untuk Bug report, Saran dan Request fitur silakan kirim ke e-mail yang terdapat diatas."
    AlwaysOnTop Me.hwnd, GetWndStyle(formMain.hwnd)
    DoEvents

End Sub

Private Sub Form_MouseMove(Button As Integer, _
                           Shift As Integer, _
                           X As Single, _
                           Y As Single)

    lblUpdate.FontUnderline = False

End Sub

Private Sub Form_Unload(Cancel As Integer)

    If chkUnload.Value = 0 Then
        Cancel = 1
        Nilai = 200
        Masuk = False
        chkUnload.Value = 1
        AlwaysOnTop Me.hwnd, False
        tmrScroll.Enabled = False
        tmrFade.Enabled = True
    End If

End Sub

Private Sub lblCap_Click(Index As Integer)

    picIn_Click

End Sub

Private Sub lblCont_Click(Index As Integer)

    picIn_Click

End Sub

Private Sub lblMain_DblClick()

    EasterFlag = True
    CurScheme = CurScheme + 1
    If CurScheme = 5 Then
        CurScheme = 1
    End If
    ChangeState CurScheme

End Sub

Private Sub lblUpdate_Click()

    If MsgBoxEx("Your browser will be opened and redirected to download page.", vbInformation + vbYesNo, "Program Update") = vbYes Then
        ShellExecute Me.hwnd, vbNullString, "http://gutz.4shared.com", vbNullString, "C:\", 1
    End If

End Sub

Private Sub lblUpdate_MouseMove(Button As Integer, _
                                Shift As Integer, _
                                X As Single, _
                                Y As Single)

    lblUpdate.FontUnderline = True

End Sub

Private Sub picIn_Click()

    tmrScroll.Enabled = Not tmrScroll.Enabled

End Sub

Private Sub Picture1_MouseMove(Button As Integer, _
                               Shift As Integer, _
                               X As Single, _
                               Y As Single)

    lblUpdate.FontUnderline = False

End Sub

Private Sub tmrFade_Timer()

    If Masuk Then
        If Nilai <= 200 Then
            SetLayered Me.hwnd, True, CByte(Nilai)
            Nilai = Nilai + 10
        Else
            SetLayered Me.hwnd, True, CByte(200)
            tmrFade.Enabled = False
        End If
    Else
        If Nilai >= 5 Then
            SetLayered Me.hwnd, True, CByte(Nilai)
            Nilai = Nilai - 10
        Else
            tmrFade.Enabled = False
            Unload Me
        End If
    End If

End Sub

Private Sub tmrScroll_Timer()

    picIn.Top = picIn.Top - 13
    If picIn.Top + picIn.Height < picUp.Height + picUp.Top Then
        picIn.Top = picOut.ScaleHeight + 20
    End If
    If EasterFlag Then
        If picIn.Top = picOut.ScaleHeight + 20 Then
            ChangeState CurScheme
            CurScheme = CurScheme + 1
            If CurScheme = 5 Then
                CurScheme = 1
            End If
        End If
    End If

End Sub


