VERSION 5.00
Begin VB.Form formDrive 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Select Drive :"
   ClientHeight    =   2295
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   1935
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2295
   ScaleWidth      =   1935
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "X"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Top             =   1830
      Width           =   375
   End
   Begin VB.ListBox lstDrive 
      Height          =   1635
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   100
      Width           =   1695
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Select"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   1830
      Width           =   1250
   End
End
Attribute VB_Name = "formDrive"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const DRV As String = "A;B;C;D;E;F;G;H;I;J;K;L;M;N;O;P;Q;R;S;T;U;V;W;X;Y;Z"
Private Const DRV_VAL As String = "1;2;4;8;16;32;64;128;256;512;1024;2048;4096;8192;16384;32768;65536;131072;262144;524288;1048576;2097152;4194304;8388608;16777216;33554432"

Private Sub cmdCancel_Click()

    Unload Me

End Sub

Private Sub Form_Load()

    GetAllDrive lstDrive
    AlwaysOnTop Me.hwnd, GetWndStyle(formMain.hwnd)
    
End Sub

Private Sub GetAllDrive(lst As ListBox)

Dim count As Integer
Dim fso As New FileSystemObject
Dim sDrive As Drive
Dim sLetter As String
Dim ArrDrv() As String
Dim ArrDrvVal() As String

    On Error Resume Next
    ArrDrv = Split(DRV, ";")
    ArrDrvVal = Split(DRV_VAL, ";")
    LockWindowUpdate lst.hwnd
    For Each sDrive In fso.Drives
        sLetter = sDrive.DriveLetter
        If Len(sDrive.VolumeName) = 0 Then
            lst.AddItem sLetter & ": [NONE]"
        Else
            lst.AddItem sLetter & ": [" & sDrive.VolumeName & "]"
        End If
        For count = 0 To 25
            If sLetter = ArrDrv(count) Then
                lst.ItemData(lst.NewIndex) = CLng(ArrDrvVal(count))
            End If
            DoEvents
        Next count
        DoEvents
    Next sDrive
    AddHScroll lst
    LockWindowUpdate 0&
    Set fso = Nothing

End Sub

Private Sub lstDrive_Click()
Debug.Print lstDrive.ItemData(lstDrive.ListIndex)
End Sub
