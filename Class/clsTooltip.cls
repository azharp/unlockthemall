VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTooltip"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Private Const WM_USER                As Long = &H400
Private Const CW_USEDEFAULT          As Long = &H80000000
Private Type Rect
    Left                                 As Long
    Top                                  As Long
    Right                                As Long
    Bottom                               As Long
End Type
Private Const TTS_NOPREFIX           As Long = &H2
Private Const TTF_CENTERTIP          As Long = &H2
Private Const TTM_ADDTOOLA           As Double = (WM_USER + 4)
Private Const TTM_UPDATETIPTEXTA     As Double = (WM_USER + 12)
Private Const TTM_SETTIPBKCOLOR      As Double = (WM_USER + 19)
Private Const TTM_SETTIPTEXTCOLOR    As Double = (WM_USER + 20)
Private Const TTM_SETTITLE           As Double = (WM_USER + 32)
Private Const TTS_BALLOON            As Long = &H40
Private Const TTS_ALWAYSTIP          As Long = &H1
Private Const TTF_SUBCLASS           As Long = &H10
Private Const TTF_IDISHWND           As Long = &H1
Private Const TTM_SETDELAYTIME       As Double = (WM_USER + 3)
Private Const TTDT_AUTOPOP           As Integer = 2
Private Const TTDT_INITIAL           As Integer = 3
Private Const TOOLTIPS_CLASSA        As String = "tooltips_class32"
Private Type TOOLINFO
    lSize                                As Long
    lFlags                               As Long
    hwnd                                 As Long
    lId                                  As Long
    lpRect                               As Rect
    hInstance                            As Long
    lpStr                                As String
    lParam                               As Long
End Type
Public Enum ttIconType
    TTNoIcon = 0
    TTIconInfo = 1
    TTIconWarning = 2
    TTIconError = 3
End Enum
#If False Then
Private TTNoIcon, TTIconInfo, TTIconWarning, TTIconError
#End If
Public Enum ttStyleEnum
    TTStandard
    TTBalloon
End Enum
#If False Then
Private TTStandard, TTBalloon
#End If
'Private mvarOnTop                    As Boolean
Private mvarDisabled                 As Boolean
Private mvarBackColor                As Long
Private mvarTitle                    As String
Private mvarForeColor                As Long
Private mvarIcon                     As ttIconType
Private mvarCentered                 As Boolean
Private mvarStyle                    As ttStyleEnum
Private mvarTipText                  As String
Private mvarVisibleTime              As Long
Private mvarDelayTime                As Long
Private m_lTTHwnd                    As Long    ' hwnd of the tooltip
Private m_lParentHwnd                As Long    ' hwnd of the window the tooltip attached to
Private ti                           As TOOLINFO
Private Declare Function CreateWindowEx Lib "user32" Alias "CreateWindowExA" (ByVal dwExStyle As Long, _
                                                                              ByVal lpClassName As String, _
                                                                              ByVal lpWindowName As String, _
                                                                              ByVal dwStyle As Long, _
                                                                              ByVal X As Long, _
                                                                              ByVal Y As Long, _
                                                                              ByVal nWidth As Long, _
                                                                              ByVal nHeight As Long, _
                                                                              ByVal hWndParent As Long, _
                                                                              ByVal hMenu As Long, _
                                                                              ByVal hInstance As Long, _
                                                                              lpParam As Any) As Long
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, _
                                                                        ByVal wMsg As Long, _
                                                                        ByVal wParam As Long, _
                                                                        lParam As Any) As Long
Private Declare Function SendMessageLong Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, _
                                                                            ByVal wMsg As Long, _
                                                                            ByVal wParam As Long, _
                                                                            ByVal lParam As Long) As Long
Private Declare Function DestroyWindow Lib "user32" (ByVal hwnd As Long) As Long

Public Property Get BackColor() As Long

    BackColor = mvarBackColor

End Property

Public Property Let BackColor(ByVal vData As Long)

    mvarBackColor = vData
    If m_lTTHwnd <> 0 Then
        SendMessage m_lTTHwnd, TTM_SETTIPBKCOLOR, mvarBackColor, 0&
    End If

End Property

Public Property Get Centered() As Boolean

    Centered = mvarCentered

End Property

Public Property Let Centered(ByVal vData As Boolean)

    mvarCentered = vData

End Property

Private Sub Class_Initialize()

    mvarDisabled = False
    mvarDelayTime = 500
    mvarVisibleTime = 5000

End Sub

Private Sub Class_Terminate()

    Destroy

End Sub

Public Function Create(ByVal ParenthWnd As Long) As Boolean

Dim lWinStyle As Long

    If Not mvarDisabled Then
        If m_lTTHwnd <> 0 Then
            DestroyWindow m_lTTHwnd
        End If
        m_lParentHwnd = ParenthWnd
        lWinStyle = TTS_ALWAYSTIP Or TTS_NOPREFIX
        If mvarStyle = TTBalloon Then
            lWinStyle = lWinStyle Or TTS_BALLOON
        End If
        m_lTTHwnd = CreateWindowEx(0&, TOOLTIPS_CLASSA, vbNullString, lWinStyle, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 0&, 0&, App.hInstance, 0&)
        'If formMain.menuOptOnTop.Checked Then
        '    AlwaysOnTop m_lTTHwnd, True
        'End If
        With ti
            If mvarCentered Then
                .lFlags = TTF_SUBCLASS Or TTF_CENTERTIP Or TTF_IDISHWND
            Else
                .lFlags = TTF_SUBCLASS Or TTF_IDISHWND
            End If
            .hwnd = m_lParentHwnd
            .lId = m_lParentHwnd
            .hInstance = App.hInstance
            '.lpstr = ALREADY SET
            '.lpRect = lpRect
            .lSize = Len(ti)
        End With
        SendMessage m_lTTHwnd, TTM_ADDTOOLA, 0&, ti
        If mvarTitle <> vbNullString Or mvarIcon <> TTNoIcon Then
            SendMessage m_lTTHwnd, TTM_SETTITLE, CLng(mvarIcon), ByVal mvarTitle
        End If
        If mvarForeColor <> Empty Then
            SendMessage m_lTTHwnd, TTM_SETTIPTEXTCOLOR, mvarForeColor, 0&
        End If
        If mvarBackColor <> Empty Then
            SendMessage m_lTTHwnd, TTM_SETTIPBKCOLOR, mvarBackColor, 0&
        End If
        SendMessageLong m_lTTHwnd, TTM_SETDELAYTIME, TTDT_AUTOPOP, mvarVisibleTime
        SendMessageLong m_lTTHwnd, TTM_SETDELAYTIME, TTDT_INITIAL, mvarDelayTime
    End If

End Function

Public Property Get DelayTime() As Long

    DelayTime = mvarDelayTime

End Property

Public Property Let DelayTime(ByVal lData As Long)

    mvarDelayTime = lData

End Property

Public Sub Destroy()

    If m_lTTHwnd <> 0 Then
        DestroyWindow m_lTTHwnd
    End If

End Sub

Public Property Get Disabled() As Boolean

    Disabled = mvarDisabled

End Property

Public Property Let Disabled(ByVal bDisabled As Boolean)

    mvarDisabled = bDisabled

End Property

Public Property Get ForeColor() As Long

    ForeColor = mvarForeColor

End Property

Public Property Let ForeColor(ByVal vData As Long)

    mvarForeColor = vData
    If m_lTTHwnd <> 0 Then
        SendMessage m_lTTHwnd, TTM_SETTIPTEXTCOLOR, mvarForeColor, 0&
    End If

End Property

Public Property Get Icon() As ttIconType

    Icon = mvarIcon

End Property

Public Property Let Icon(ByVal vData As ttIconType)

    mvarIcon = vData
    If m_lTTHwnd <> 0 Then
        If mvarTitle <> Empty Then
            If mvarIcon <> TTNoIcon Then
                SendMessage m_lTTHwnd, TTM_SETTITLE, CLng(mvarIcon), ByVal mvarTitle
            End If
        End If
    End If

End Property

Public Property Get Style() As ttStyleEnum

    Style = mvarStyle

End Property

Public Property Let Style(ByVal vData As ttStyleEnum)

    mvarStyle = vData

End Property

Public Property Get TipText() As String

    TipText = mvarTipText

End Property

Public Property Let TipText(ByVal vData As String)

    mvarTipText = vData
    ti.lpStr = vData
    If m_lTTHwnd <> 0 Then
        SendMessage m_lTTHwnd, TTM_UPDATETIPTEXTA, 0&, ti
    End If

End Property

Public Property Get Title() As String

    Title = ti.lpStr

End Property

Public Property Let Title(ByVal vData As String)

    mvarTitle = vData
    If m_lTTHwnd <> 0 Then
        If mvarTitle <> Empty Then
            If mvarIcon <> TTNoIcon Then
                SendMessage m_lTTHwnd, TTM_SETTITLE, CLng(mvarIcon), ByVal mvarTitle
            End If
        End If
    End If

End Property

Public Property Get VisibleTime() As Long

    VisibleTime = mvarVisibleTime

End Property

Public Property Let VisibleTime(ByVal lData As Long)

    mvarVisibleTime = lData

End Property


