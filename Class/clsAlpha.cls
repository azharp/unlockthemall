VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAlpha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
Private Type RECT
    Left                             As Long
    Top                              As Long
    Right                            As Long
    Bottom                           As Long
End Type
Private Const WS_EX_LAYERED      As Long = &H80000
Private Const GWL_EXSTYLE        As Long = (-20)
Private Const LWA_ALPHA          As Long = &H2
Private Const RDW_ALLCHILDREN    As Long = &H80
Private Const RDW_ERASE          As Long = &H4
Private Const RDW_FRAME          As Long = &H400
Private Const RDW_INVALIDATE     As Long = &H1
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, _
                                                                            ByVal nIndex As Long, _
                                                                            ByVal dwNewLong As Long) As Long
Private Declare Function RedrawWindow Lib "user32" (ByVal hwnd As Long, _
                                                    lprcUpdate As RECT, _
                                                    ByVal hrgnUpdate As Long, _
                                                    ByVal fuRedraw As Long) As Long
Private Declare Function SetLayeredWindowAttributes Lib "user32" (ByVal hwnd As Long, _
                                                                  ByVal crKey As Long, _
                                                                  ByVal bAlpha As Byte, _
                                                                  ByVal dwFlags As Long) As Long

Friend Sub ReleaseDisplay(ByVal lngHWnd As Long)

    SetLayered lngHWnd, False, 255

End Sub

Friend Sub SetLayered(ByVal lngHWnd As Long, _
                      ByVal bolSetAs As Boolean, _
                      ByVal bAlpha As Byte)

Dim nullRect As RECT
Dim lRet     As Long

    lRet = GetWindowLong(lngHWnd, GWL_EXSTYLE)
    If bolSetAs Then
        lRet = lRet Or WS_EX_LAYERED
    Else
        lRet = lRet And Not WS_EX_LAYERED
    End If
    SetWindowLong lngHWnd, GWL_EXSTYLE, lRet
    If bolSetAs Then
        SetLayeredWindowAttributes lngHWnd, 0, bAlpha, LWA_ALPHA
    Else
        RedrawWindow lngHWnd, nullRect, 0&, RDW_ALLCHILDREN Or RDW_ERASE Or RDW_FRAME Or RDW_INVALIDATE
    End If

End Sub


