VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsRegistry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private Const ERROR_SUCCESS              As Long = 0
Private Const ERROR_NO_MORE_ITEMS        As Long = 259
Private Const KEY_QUERY_VALUE            As Long = &H1
Private Const KEY_SET_VALUE              As Long = &H2
Private Const KEY_CREATE_SUB_KEY         As Long = &H4
Private Const KEY_ENUMERATE_SUB_KEYS     As Long = &H8
Private Const KEY_NOTIFY                 As Long = &H10
Private Const KEY_CREATE_LINK            As Long = &H20
Private Const READ_CONTROL               As Long = &H20000
Private Const SYNCHRONIZE                As Long = &H100000
Private Const STANDARD_RIGHTS_READ       As Long = READ_CONTROL
Private Const STANDARD_RIGHTS_WRITE      As Long = READ_CONTROL
Private Const STANDARD_RIGHTS_ALL        As Long = &H1F0000
Private Const KEY_READ                   As Double = STANDARD_RIGHTS_READ Or KEY_QUERY_VALUE Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY
Private Const KEY_WRITE                  As Double = STANDARD_RIGHTS_WRITE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY
Private Const KEY_ALL_ACCESS             As Double = (STANDARD_RIGHTS_ALL Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE)
Public Enum rcMainKey
    HKEY_CLASSES_ROOT = &H80000000
    HKEY_CURRENT_USER = &H80000001
    HKEY_LOCAL_MACHINE = &H80000002
    HKEY_USERS = &H80000003
End Enum
#If False Then
Private HKEY_CLASSES_ROOT, HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE, HKEY_USERS, HKEY_PERFORMANCE_DATA, HKEY_CURRENT_CONFIG
Private HKEY_DYN_DATA
#End If
Public Enum rcRegType
    REG_NONE = 0
    REG_SZ = 1
    REG_EXPAND_SZ = 2
    REG_BINARY = 3
    REG_DWORD = 4
    REG_DWORD_LITTLE_ENDIAN = 4
    REG_DWORD_BIG_ENDIAN = 5
    REG_LINK = 6
    REG_MULTI_SZ = 7
    REG_RESOURCE_LIST = 8
    REG_FULL_RESOURCE_DESCRIPTOR = 9
    REG_RESOURCE_REQUIREMENTS_LIST = 10
End Enum
#If False Then
Private REG_NONE, REG_SZ, REG_EXPAND_SZ, REG_BINARY, REG_DWORD, REG_DWORD_LITTLE_ENDIAN, REG_DWORD_BIG_ENDIAN, REG_LINK, REG_MULTI_SZ
Private REG_RESOURCE_LIST, REG_FULL_RESOURCE_DESCRIPTOR, REG_RESOURCE_REQUIREMENTS_LIST
#End If
Private hKey                             As Long
Private mainKey                          As Long
Private sKey                             As String
Private createNoExists                   As Boolean
Private Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" (ByVal hKey As Long, _
                                                                            ByVal lpSubKey As String, _
                                                                            phkResult As Long) As Long
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" (ByVal hKey As Long, _
                                                                                ByVal lpSubKey As String, _
                                                                                ByVal ulOptions As Long, _
                                                                                ByVal samDesired As Long, _
                                                                                phkResult As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long
Private Declare Function RegCreateKey Lib "advapi32.dll" Alias "RegCreateKeyA" (ByVal hKey As Long, _
                                                                                ByVal lpSubKey As String, _
                                                                                phkResult As Long) As Long
Private Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" (ByVal hKey As Long, _
                                                                                ByVal dwIndex As Long, _
                                                                                ByVal lpValueName As String, _
                                                                                lpcbValueName As Long, _
                                                                                ByVal lpReserved As Long, _
                                                                                lpType As Long, _
                                                                                lpData As Any, _
                                                                                lpcbData As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias "RegQueryValueExA" (ByVal hKey As Long, _
                                                                                      ByVal lpValueName As String, _
                                                                                      ByVal lpReserved As Long, _
                                                                                      lpType As Long, _
                                                                                      ByVal lpData As String, _
                                                                                      lpcbData As Long) As Long
Private Declare Function RegQueryValueExA Lib "advapi32.dll" (ByVal hKey As Long, _
                                                              ByVal lpValueName As String, _
                                                              ByVal lpReserved As Long, _
                                                              lpType As Long, _
                                                              ByRef lpData As Long, _
                                                              lpcbData As Long) As Long
Private Declare Function RegSetValueEx Lib "advapi32.dll" Alias "RegSetValueExA" (ByVal hKey As Long, _
                                                                                  ByVal lpValueName As String, _
                                                                                  ByVal Reserved As Long, _
                                                                                  ByVal dwType As Long, _
                                                                                  ByVal lpData As String, _
                                                                                  ByVal cbData As Long) As Long
Private Declare Function RegSetValueExA Lib "advapi32.dll" (ByVal hKey As Long, _
                                                            ByVal lpValueName As String, _
                                                            ByVal Reserved As Long, _
                                                            ByVal dwType As Long, _
                                                            ByRef lpData As Long, _
                                                            ByVal cbData As Long) As Long
Private Declare Function RegDeleteValue Lib "advapi32.dll" Alias "RegDeleteValueA" (ByVal hKey As Long, _
                                                                                    ByVal lpValueName As String) As Long

Private Sub Class_Initialize()

    CreateKeyIfDoesntExists = True

End Sub

Public Function CreateKey(ByVal sPath As String) As Long

    hKey = GetKeys(sPath, sKey)
    If RegCreateKey(hKey, sKey, mainKey) = ERROR_SUCCESS Then
        RegCloseKey mainKey
        CreateKey = mainKey
    Else
        CreateKey = 0
    End If

End Function

Public Property Get CreateKeyIfDoesntExists() As Boolean

    CreateKeyIfDoesntExists = createNoExists

End Property

Public Property Let CreateKeyIfDoesntExists(ByVal offon As Boolean)

    createNoExists = offon

End Property

Public Function EnumValues(ByVal sPath As String, _
                           sName() As String, _
                           sValue() As Variant, _
                           Optional OnlyType As rcRegType = -1) As Long

Dim mainKey As Long
Dim rName   As String
Dim Cnt     As Long
Dim rData   As String
Dim rType   As Long
Dim RetData As Long
Dim RetVal  As Long

    hKey = GetKeys(sPath, sKey)
    If RegOpenKey(hKey, sKey, mainKey) = ERROR_SUCCESS Then
        rName = Space$(255)
        rData = Space$(255)
        RetVal = 255
        RetData = 255
        Erase sName
        Erase sValue
        Do While RegEnumValue(mainKey, Cnt, rName, RetVal, 0, rType, ByVal rData, RetData) <> ERROR_NO_MORE_ITEMS
            If OnlyType = -1 Or OnlyType = rType Then
                ReDim Preserve sName(EnumValues) As String
                ReDim Preserve sValue(EnumValues) As Variant
                sName(EnumValues) = Trim$(Left$(rName, RetVal))
                If rType = REG_DWORD Then
                    sValue(EnumValues) = ReadDWORD(sPath, sName(EnumValues), 0)
                ElseIf rType = REG_SZ Then
                    sValue(EnumValues) = ReadString(sPath, sName(EnumValues), vbNullString)
                End If
                EnumValues = EnumValues + 1
            End If
            Cnt = Cnt + 1
            rName = Space$(255)
            rData = Space$(255)
            RetVal = 255
            RetData = 255
        Loop
        RegCloseKey hKey
    Else
        EnumValues = -1
    End If

End Function

Private Function GetKeys(ByVal sPath As String, _
                         sKey As String) As rcMainKey

Dim pos As Long
Dim mk  As String

    pos = InStr(1, sPath, "\")
    If pos = 0 Then
        mk = UCase$(sPath)
        sKey = vbNullString
    Else
        mk = UCase$(Left$(sPath, 4))
        sKey = Right$(sPath, Len(sPath) - pos)
    End If
    Select Case mk
    Case "HKCU"
        GetKeys = HKEY_CURRENT_USER
    Case "HKLM"
        GetKeys = HKEY_LOCAL_MACHINE
    Case "HKCR"
        GetKeys = HKEY_CLASSES_ROOT
    Case "HKUS"
        GetKeys = HKEY_USERS
    End Select

End Function

Public Function KeyExists(ByVal sPath As String) As Boolean

    hKey = GetKeys(sPath, sKey)
    If RegOpenKeyEx(hKey, sKey, 0, KEY_READ, mainKey) = ERROR_SUCCESS Then
        KeyExists = True
        RegCloseKey mainKey
    Else
        KeyExists = False
    End If

End Function

Public Function KillValue(ByVal sPath As String, _
                          ByVal sName As String) As Long

    hKey = GetKeys(sPath, sKey)
    If RegOpenKeyEx(hKey, sKey, 0, KEY_ALL_ACCESS, mainKey) = ERROR_SUCCESS Then
        RegDeleteValue mainKey, sName
        RegCloseKey mainKey
        KillValue = mainKey
    Else
        KillValue = 0
    End If

End Function

Public Function ReadDWORD(ByVal sPath As String, _
                          ByVal sName As String, _
                          Optional lDefault As Long = -1) As Long

Dim lData As Long

    hKey = GetKeys(sPath, sKey)
    If RegOpenKeyEx(hKey, sKey, 0, KEY_READ, mainKey) = ERROR_SUCCESS Then
        If RegQueryValueExA(mainKey, sName, 0, REG_DWORD, lData, 4) = ERROR_SUCCESS Then
            RegCloseKey mainKey
            ReadDWORD = lData
        Else
            ReadDWORD = lDefault
        End If
    Else
        ReadDWORD = lDefault
    End If

End Function

Public Function ReadString(ByVal sPath As String, _
                           ByVal sName As String, _
                           Optional sDefault As String = vbNullChar) As String

Dim sData As String

    hKey = GetKeys(sPath, sKey)
    If RegOpenKeyEx(hKey, sKey, 0, KEY_READ, mainKey) = ERROR_SUCCESS Then
        sData = Space$(255)
        If RegQueryValueEx(mainKey, sName, 0, REG_SZ, sData, Len(sData)) = ERROR_SUCCESS Then
            RegCloseKey mainKey
            sData = Trim$(sData)
            If Len(sData) > 0 Then
                ReadString = Left$(sData, Len(sData) - 1)
            Else
                ReadString = sData
            End If
        Else
            ReadString = sDefault
        End If
    Else
        ReadString = sDefault
    End If

End Function

Public Function ValueExists(ByVal sPath As String, _
                            ByVal sName As String) As Boolean

Dim sData As String

    hKey = GetKeys(sPath, sKey)
    If RegOpenKeyEx(hKey, sKey, 0, KEY_READ, mainKey) = ERROR_SUCCESS Then
        If RegQueryValueEx(mainKey, sName, 0, 0, sData, 1) = ERROR_SUCCESS Then
            RegCloseKey mainKey
            ValueExists = True
        Else
            ValueExists = False
        End If
    Else
        ValueExists = False
    End If

End Function

Public Function WriteDWORD(ByVal sPath As String, _
                           ByVal sName As String, _
                           ByVal lValue As Long) As Long

    If Not KeyExists(sPath) Then
        If createNoExists Then
            CreateKey sPath
        Else
            WriteDWORD = 0
            Exit Function
        End If
    End If
    hKey = GetKeys(sPath, sKey)
    If RegOpenKeyEx(hKey, sKey, 0, KEY_WRITE, mainKey) = ERROR_SUCCESS Then
        If RegSetValueExA(mainKey, sName, 0, REG_DWORD, lValue, 4) = ERROR_SUCCESS Then
            RegCloseKey mainKey
            WriteDWORD = mainKey
        Else
            WriteDWORD = 0
        End If
    Else
        WriteDWORD = 0
    End If

End Function

Public Function WriteString(ByVal sPath As String, _
                            ByVal sName As String, _
                            ByVal sValue As String) As Long

    If Not KeyExists(sPath) Then
        If createNoExists Then
            CreateKey sPath
        Else
            WriteString = 0
            Exit Function
        End If
    End If
    hKey = GetKeys(sPath, sKey)
    If sName = "@" Then
        sName = vbNullString
    End If
    If RegOpenKeyEx(hKey, sKey, 0, KEY_WRITE, mainKey) = ERROR_SUCCESS Then
        If RegSetValueEx(mainKey, sName, 0, REG_SZ, ByVal sValue, Len(sValue)) = ERROR_SUCCESS Then
            RegCloseKey mainKey
            WriteString = mainKey
        Else
            WriteString = 0
        End If
    Else
        WriteString = 0
    End If

End Function


