Attribute VB_Name = "modShortcut"
Option Explicit
Public Enum ShortcutWindowState
    wsNormal = 1
    wsMaximized = 3
    wsMinimized = 7
End Enum
#If False Then
Private wsNormal, wsMaximized, wsMinimized
#End If


Public Function CreateShortcut(ByRef FullPathOfShortcut As String, _
                               ByVal DestPath As String, _
                               Optional ByVal sArguments As String, _
                               Optional ByVal sDescription As String, _
                               Optional ByVal sHotkey As String, _
                               Optional sWindowState As ShortcutWindowState = wsNormal, _
                               Optional ByVal sWorkingDirectory As String, _
                               Optional ByVal sIconLocation As String, _
                               Optional ByVal nIconLocationIndex As Long = 0) As Boolean

Dim oShell    As New Shell
Dim f         As Folder
Dim fItem     As FolderItem
Dim shellLink As ShellLinkObject

    On Error GoTo ErrHand
    If UCase$(Right$(FullPathOfShortcut, 3)) <> "LNK" Then
        FullPathOfShortcut = FullPathOfShortcut & ".lnk"
    End If
    If CreateShortcutTemplate(FullPathOfShortcut) Then
        Set f = oShell.NameSpace(ssfDRIVES)
        Set fItem = f.ParseName(FullPathOfShortcut)
        If Not fItem Is Nothing Then
            Set shellLink = fItem.GetLink
            With shellLink
                .Path = DestPath
                .Arguments = sArguments
                .Description = sDescription
                If LenB(sHotkey) Then
                    .Hotkey = sHotkey
                End If
                .ShowCommand = sWindowState
                .WorkingDirectory = sWorkingDirectory
                If LenB(sIconLocation) Then
                    .SetIconLocation sIconLocation, nIconLocationIndex
                End If
                .Save
            End With
            CreateShortcut = True
        End If
    End If

Exit Function

ErrHand:
    If LenB(Dir(FullPathOfShortcut)) Then
        Kill FullPathOfShortcut
    End If
    CreateShortcut = False

End Function

Public Function CreateShortcutTemplate(ByVal sFilePath As String) As Boolean

Dim lHandle   As Long
Dim bytData() As Byte

    On Error GoTo Hell
    bytData = LoadResData(101, "99")
    lHandle = FreeFile
    Open sFilePath For Binary Access Write As #lHandle
    Put #lHandle, , bytData
    Close #lHandle
    CloseHandle lHandle
    CreateShortcutTemplate = True

Exit Function

Hell:
    CreateShortcutTemplate = False

End Function


