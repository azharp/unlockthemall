Attribute VB_Name = "modPriority"
Option Explicit
Private Const REALTIME_PRIORITY_CLASS         As Long = &H100
Private Const HIGH_PRIORITY_CLASS             As Long = &H80
Private Const ABOVE_NORMAL_PRIORITY_CLASS     As Long = 32768
Private Const NORMAL_PRIORITY_CLASS           As Long = &H20
Private Const BELOW_NORMAL_PRIORITY_CLASS     As Long = &H4000
Private Const LOW_PRIORITY_CLASS              As Long = &H40
Private Const STANDARD_RIGHTS_REQUIRED        As Long = &HF0000
Private Const SYNCHRONIZE                     As Long = &H100000
Private Const PROCESS_ALL_ACCESS              As Long = (STANDARD_RIGHTS_REQUIRED Or SYNCHRONIZE Or &HFFF)
Private Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDesiredAccess As Long, _
                                                         ByVal bInheritHandle As Long, _
                                                         ByVal dwProcessId As Long) As Long
Private Declare Function GetPriorityClass Lib "kernel32" (ByVal hProcess As Long) As Long
Private Declare Function SetPriorityClass Lib "kernel32" (ByVal hProcess As Long, _
                                                          ByVal dwPriorityClass As Long) As Long

Public Function GetProcessPriority(ByVal lPID As Long) As Long

Dim lRet    As Long
Dim lHandle As Long

    On Error GoTo ErrHand
    lHandle = OpenProcess(PROCESS_ALL_ACCESS, 0, lPID)
    Select Case GetPriorityClass(lHandle)
    Case LOW_PRIORITY_CLASS
        lRet = 5
    Case BELOW_NORMAL_PRIORITY_CLASS
        lRet = 4
    Case NORMAL_PRIORITY_CLASS
        lRet = 3
    Case ABOVE_NORMAL_PRIORITY_CLASS
        lRet = 2
    Case HIGH_PRIORITY_CLASS
        lRet = 1
    Case REALTIME_PRIORITY_CLASS
        lRet = 0
    Case Else
        lRet = -1
    End Select
    GetProcessPriority = lRet
    CloseHandle lHandle
    
Exit Function
ErrHand:
    GetProcessPriority = 0
    CloseHandle lHandle
    
End Function

Public Function SetProcessPriority(ByVal lPID As Long, _
                                   Optional ByVal lLevel As Single = 3) As Long

Dim lHandle  As Long
Dim tmpConst As Long

    On Error GoTo ErrHand
    Select Case lLevel
    Case 0
        tmpConst = REALTIME_PRIORITY_CLASS
    Case 1
        tmpConst = HIGH_PRIORITY_CLASS
    Case 2
        tmpConst = ABOVE_NORMAL_PRIORITY_CLASS
    Case 3
        tmpConst = NORMAL_PRIORITY_CLASS
    Case 4
        tmpConst = BELOW_NORMAL_PRIORITY_CLASS
    Case 5
        tmpConst = LOW_PRIORITY_CLASS
    End Select
    lHandle = OpenProcess(PROCESS_ALL_ACCESS, 0, lPID)
    If GetPriorityClass(lHandle) = tmpConst Then
        SetProcessPriority = 1
    Else
        SetProcessPriority = SetPriorityClass(lHandle, tmpConst)
    End If
    CloseHandle lHandle
    
Exit Function
ErrHand:
    SetProcessPriority = 0
    CloseHandle lHandle

End Function
