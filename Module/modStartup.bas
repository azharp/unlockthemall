Attribute VB_Name = "modStartup"
Option Explicit
Private Reg              As New clsRegistry
Private Const RUN_HKLM   As String = "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run"
Public Const RUN_HKCU    As String = "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Run"

Public Function AddStartupItem(ByVal sPath As String) As Boolean

    On Error GoTo ErrHand
    Select Case formMain.cmbType.ListIndex
    Case 0
        AddStartupItem = Reg.WriteString(RUN_HKLM, GetFileName(sPath), sPath)
    Case 1
        AddStartupItem = Reg.WriteString(RUN_HKCU, GetFileName(sPath), sPath)
    Case 2
        AddStartupItem = CreateShortcut(AppVar.StrAll & "\" & GetBaseName(sPath), sPath, , , , , GetFilePath(sPath))
    Case 3
        AddStartupItem = CreateShortcut(AppVar.StrCurr & "\" & GetBaseName(sPath), sPath, , , , , GetFilePath(sPath))
    End Select

Exit Function

ErrHand:
    AddStartupItem = False

End Function

Public Function DeleteEntry(ByVal sType As Single, _
                            Optional ByVal RegName As String, _
                            Optional ByVal RegValue As String) As Long

Dim lRet As Long

    Select Case sType
    Case 0
        lRet = Reg.KillValue(RUN_HKLM, RegName)
    Case 1
        lRet = Reg.KillValue(RUN_HKCU, RegName)
    Case 2, 3
        lRet = RecycleBin(RegValue, False)
    End Select
    DeleteEntry = lRet

End Function

Public Sub GetAllStartup(Optional ByVal Mode As Single = 0)

    SysVar.Startup = 0
    GetRegAll Mode
    GetRegCurrent Mode
    GetStartupAll Mode
    GetStartupCurrent Mode

End Sub

Public Sub GetRegAll(Optional ByVal Mode As Single = 0)

Dim i          As Long
Dim ArrName()  As String
Dim ArrValue() As Variant
Dim RetVal     As Long

    On Error GoTo ErrHand
    If Mode = 0 Then
        formMain.lstStartup(0).Clear
        formMain.lstStartup(1).Clear
    End If
    RetVal = Reg.EnumValues(RUN_HKLM, ArrName, ArrValue, REG_SZ)
    If RetVal Then
        For i = 0 To UBound(ArrName)
            If Len(ArrName(i)) > 0 Then
                If Mode = 0 Then
                    formMain.lstStartup(0).AddItem ArrName(i)
                    formMain.lstStartup(1).AddItem ArrValue(i)
                End If
                If Mode = 2 Then
                    FillStrInfo SysVar.Startup, "RegAll", ArrName(i), RUN_HKLM
                End If
                SysVar.Startup = SysVar.Startup + 1
            End If
            DoEvents
        Next i
    End If

Exit Sub

ErrHand:
    ErrorHandler Err, "GetRegAll"
    Resume Next

End Sub

Public Sub GetRegCurrent(Optional ByVal Mode As Single = 0)

Dim i          As Long
Dim ArrName()  As String
Dim ArrValue() As Variant
Dim RetVal     As Long

    On Error GoTo ErrHand
    If Mode = 0 Then
        formMain.lstStartup(0).Clear
        formMain.lstStartup(1).Clear
    End If
    RetVal = Reg.EnumValues(RUN_HKCU, ArrName, ArrValue, REG_SZ)
    If RetVal Then
        For i = 0 To UBound(ArrName)
            If Len(ArrName(i)) > 0 Then
                If Mode = 0 Then
                    formMain.lstStartup(0).AddItem ArrName(i)
                    formMain.lstStartup(1).AddItem ArrValue(i)
                End If
                If Mode = 2 Then
                    FillStrInfo SysVar.Startup, "RegCur", ArrName(i), RUN_HKCU
                End If
                SysVar.Startup = SysVar.Startup + 1
            End If
            DoEvents
        Next i
    End If

Exit Sub

ErrHand:
    ErrorHandler Err, "GetRegCurrent"
    Resume Next

End Sub

Public Sub GetStartupAll(Optional ByVal Mode As Single = 0)

Dim i      As Long
Dim tmpStr As String

    On Error GoTo ErrHand
    formMain.lstFile.Refresh
    If Mode = 0 Then
        formMain.lstStartup(0).Clear
        formMain.lstStartup(1).Clear
    End If
    formMain.lstFile.Path = AppVar.StrAll
    For i = 0 To formMain.lstFile.ListCount - 1
        If Mode = 0 Then
            formMain.lstStartup(0).AddItem formMain.lstFile.List(i)
        End If
        If Mode = 2 Then
            tmpStr = AppVar.StrAll & "\" & formMain.lstFile.List(i)
            FillStrInfo SysVar.Startup, "FolAll", formMain.lstFile.List(i), tmpStr
        End If
        If UCase$(Right$(formMain.lstFile.List(i), 4)) = UCase$(".lnk") Then
            tmpStr = GetLinkInfo(AppVar.StrAll & "\" & formMain.lstFile.List(i))
            If Mode = 0 Then
                formMain.lstStartup(1).AddItem tmpStr
            End If
        Else
            tmpStr = AppVar.StrAll & "\" & formMain.lstFile.List(i)
            If Mode = 0 Then
                formMain.lstStartup(1).AddItem tmpStr
            End If
        End If
        SysVar.Startup = SysVar.Startup + 1
        DoEvents
    Next i

Exit Sub

ErrHand:
    ErrorHandler Err, "GetStartupAll"
    Resume Next

End Sub

Public Sub GetStartupCurrent(Optional ByVal Mode As Single = 0)

Dim i      As Long
Dim tmpStr As String

    On Error GoTo ErrHand
    formMain.lstFile.Refresh
    If Mode = 0 Then
        formMain.lstStartup(0).Clear
        formMain.lstStartup(1).Clear
    End If
    formMain.lstFile.Path = AppVar.StrCurr
    For i = 0 To formMain.lstFile.ListCount - 1
        If Mode = 0 Then
            formMain.lstStartup(0).AddItem formMain.lstFile.List(i)
        End If
        If Mode = 2 Then
            tmpStr = AppVar.StrCurr & "\" & formMain.lstFile.List(i)
            FillStrInfo SysVar.Startup, "FolCur", formMain.lstFile.List(i), tmpStr
        End If
        If UCase$(Right$(formMain.lstFile.List(i), 4)) = UCase$(".lnk") Then
            tmpStr = GetLinkInfo(AppVar.StrCurr & "\" & formMain.lstFile.List(i))
            If Mode = 0 Then
                formMain.lstStartup(1).AddItem tmpStr
            End If
        Else
            tmpStr = AppVar.StrCurr & "\" & formMain.lstFile.List(i)
            If Mode = 0 Then
                formMain.lstStartup(1).AddItem tmpStr
            End If
        End If
        SysVar.Startup = SysVar.Startup + 1
        DoEvents
    Next i

Exit Sub

ErrHand:
    ErrorHandler Err, "GetStartupCurrent"
    Resume Next

End Sub

Public Sub MoveStartup(ByVal sType As Single, _
                       ByVal Newloc As Single, _
                       Optional ByVal RegName1 As String, _
                       Optional ByVal RegName2 As String, _
                       Optional ByVal RegValue As String)

Dim lRet As Boolean

    If RegValue <> vbNullString Then
        Select Case sType
        Case 0, 1
            Select Case Newloc
            Case 1
                lRet = Reg.WriteString(RUN_HKLM, RegName1, RegValue)
                If Not lRet Then
                    GoTo MoveError1
                Else
                    lRet = Reg.KillValue(RUN_HKCU, RegName1)
                    If Not lRet Then
                        GoTo MoveError1
                    End If
                End If
            Case 2
                lRet = Reg.WriteString(RUN_HKCU, RegName1, RegValue)
                If Not lRet Then
                    GoTo MoveError1
                Else
                    lRet = Reg.KillValue(RUN_HKLM, RegName1)
                    If Not lRet Then
                        GoTo MoveError1
                    End If
                End If
            End Select
        Case 2, 3
            Select Case Newloc
            Case 1
                lRet = MoveFile(RegValue, AppVar.StrAll & "\" & RegName2)
                If Not lRet Then
                    GoTo MoveError2
                End If
            Case 2
                lRet = MoveFile(RegValue, AppVar.StrCurr & "\" & RegName2)
                If Not lRet Then
                    GoTo MoveError2
                End If
            End Select
        End Select
    End If

Exit Sub

MoveError1:
    MsgBoxEx "Cannot move selected startup item!" & vbNewLine & _
       vbNewLine & _
       "Item name : " & RegName1, vbCritical, "Move Item"

Exit Sub

MoveError2:
    MsgBoxEx "Cannot move selected startup item!" & vbNewLine & _
       vbNewLine & _
       "Item name : " & RegName2, vbCritical, "Move Item"

End Sub


