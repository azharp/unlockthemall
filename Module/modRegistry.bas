Attribute VB_Name = "modRegistry"
Option Explicit
Private Reg                  As New clsRegistry
Private Const CHK_NAME       As String = "DisableRegistryTools;NoFolderOptions;DisableCMD;DisableTaskMgr;NoMyComputer;NoUserNameInStartMenu;NoStartMenuMorePrograms;NoSMHelp;NoFind;NoRun;StartmenuLogoff;NoClose;NoChangeStartMenu;NoSimpleStartMenu;LockTaskbar;NoSetTaskbar;NoTrayContextMenu;HideClock;NoTrayItemsDisplay;NoControlPanel;ClearRecentDocsOnExit;NoDriveTypeAutoRun;NoShellSearchButton;NoFileMenu;NoViewContextMenu;NoDrives;NoViewOnDrive;NoManageMyComputerVerb;NoDesktop;NoDispCpl;NoCloseDragDropBands;NoMovingBands;NoToolbarCustomize;NoSaveSettings;NoNetHood;NoEntireNetwork;NoWorkgroupContents;NoNetSetup"
Private Const CHK_CAPTION    As String = "No Registry Editor;Hide Folder Options;Disable CMD;No Task Manager;Hide My Computer;Hide Username;Hide All Programs;Hide Startmenu Help;Hide Startmenu Find;Hide Startmenu Run;Hide Log Off Button;Hide Shutdown Button;No Change Startmenu;Classic Startmenu;Lock Taskbar;No Change Taskbar;No Taskbar Menu;Hide Taskbar Clock;Hide Tray Icon;Hide Control Panel;Clear Recent Docs;No Drive Autorun;Hide Search Menu;Hide File Menu;No Context Menu;Hide C:\ Drive;Restrict C:\ Drive;No Manage Menu;Hide Desktop Icon;No Display Properties;No Add Toolbar;No Change Toolbar;Hide Custom Toolbar;No Save Settings;Hide Network Places;Hide Entire Network;Hide Workgroups;Hide Network Setup"
Private Const CHK_TIPS As String = "Disable Windows internal Registry Editor;Remove Folder Options menu in Windows Explorer;Disable access to Windows Command Prompt;Disable Windows Task Manager;Remove My Computer in Start menu and Windows Explorer;Remove current username in Start menu;Remove All Programs list in Start menu;Remove Help and Support button in Start menu;Remove Search button in Start menu;Remove Run button in Start menu;Remove Log Off button in Start menu;Remove Turn Off Computer button in Start menu;Prevent user from changing Start menu order;Force Windows to use Classic Start menu;Lock Windows Taskbar;Prevent user from changing Taskbar settings;Disable Taskbar context menu;Remove Clock from Tray Icon area;Remove Tray Icon area" & _
                                    ";Remove Control Panel from Start menu;Clear Recent Documents in Start menu on exit;Disable Windows Autorun function;Remove Search button in Windows Explorer;Remove File menu in Windows Explorer;Disable context menu of Windows Explorer;Hide C:\ drive;Prevent user from accessing C:\ drive;Remove Manage menu from My Computer;Remove all icons from Desktop;Prevent user from accessing Display Properties;Prevent user adding/changing toolbar on Taskbar;Prevent user changing toolbar position on Desktop;Hide all Custom toolbar on Desktop;Do not remember individual folder view settings;Remove Network Places in Windows Explorer;Remove Entire Network in Network Places;Remove Workgroups in Entire Network;Prevent user from changing Network settings"
Public Const KEY_EXPLORER    As String = "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
Public Const KEY_SYSTEM      As String = "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System"
Public Const KEY_NETWORK     As String = "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Network"
Public RegNameArr()          As String
Public RegCaptionArr()       As String
Public RegTipsArr()          As String

Public Sub GetRegValue(ByVal sName As CheckBox, _
                       ByVal Mode As Single)


Dim chkName As String

    chkName = RegNameArr(sName.Index)
    Select Case chkName
    Case "DisableRegistryTools", "DisableTaskMgr", "NoDispCpl"
        Select Case Reg.ReadDWORD(KEY_SYSTEM, chkName, 0)
        Case 1
            If Mode = 0 Then
                sName.Value = 1
            End If
            SysVar.Registry = SysVar.Registry + 1
        Case 0
            If Mode = 0 Then
                sName.Value = 0
            End If
        End Select
    Case "NoEntireNetwork", "NoWorkgroupContents", "NoNetSetup"
        Select Case Reg.ReadDWORD(KEY_NETWORK, chkName, 0)
        Case 1
            If Mode = 0 Then
                sName.Value = 1
            End If
            SysVar.Registry = SysVar.Registry + 1
        Case 0
            If Mode = 0 Then
                sName.Value = 0
            End If
        End Select
    Case "LockTaskbar", "NoDrives", "NoViewOnDrive", "NoSimpleStartMenu", "NoSaveSettings"
        Select Case Reg.ReadDWORD(KEY_EXPLORER, chkName, 0)
        Case 1, 4
            If Mode = 0 Then
                sName.Value = 1
            End If
            SysVar.Registry = SysVar.Registry + 1
        Case 0
            If Mode = 0 Then
                sName.Value = 0
            End If
        End Select
    Case "DisableCMD"
        Select Case Reg.ReadDWORD("HKCU\Software\Policies\Microsoft\Windows\System", "DisableCMD", 0)
        Case 1
            If Mode = 0 Then
                sName.Value = 1
            End If
            SysVar.Registry = SysVar.Registry + 1
        Case 0
            If Mode = 0 Then
                sName.Value = 0
            End If
        End Select
    Case "NoMyComputer"
        Select Case Reg.ReadDWORD("HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\NonEnum", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 0)
        Case 1
            If Mode = 0 Then
                sName.Value = 1
            End If
            SysVar.Registry = SysVar.Registry + 1
        Case 0
            If Mode = 0 Then
                sName.Value = 0
            End If
        End Select
    Case Else
        Select Case Reg.ReadDWORD(KEY_EXPLORER, chkName, 0)
        Case 1, 4, 127
            If Mode = 0 Then
                sName.Value = 1
            End If
            SysVar.Registry = SysVar.Registry + 1
        Case 0
            If Mode = 0 Then
                sName.Value = 0
            End If
        End Select
    End Select

End Sub

Public Sub GetValue(Optional ByVal Mode As Single = 0)

Dim i As Integer

    SysVar.Registry = 0
    For i = 0 To 37
        GetRegValue formMain.chkRegArr(i), Mode
        DoEvents
    Next i

End Sub

Public Sub SetRegValue(ByVal sName As String, _
                       ByVal bValue As Boolean)

    Select Case sName
    Case "DisableRegistryTools", "DisableTaskMgr", "NoDispCpl"
        If bValue Then
            Reg.WriteDWORD KEY_SYSTEM, sName, 1
        Else
            Reg.KillValue KEY_SYSTEM, sName
        End If
    Case "NoEntireNetwork", "NoWorkgroupContents", "NoNetSetup"
        If bValue Then
            Reg.WriteDWORD KEY_NETWORK, sName, 1
        Else
            Reg.KillValue KEY_NETWORK, sName
        End If
    Case "NoDrives", "NoViewOnDrive"
        If bValue Then
            Reg.WriteDWORD KEY_EXPLORER, sName, 4
        Else
            Reg.KillValue KEY_EXPLORER, sName
        End If
    Case "DisableCMD"
        If bValue Then
            Reg.WriteDWORD "HKCU\Software\Policies\Microsoft\Windows\System", sName, 1
        Else
            Reg.KillValue "HKCU\Software\Policies\Microsoft\Windows\System", sName
        End If
    Case "NoDriveTypeAutoRun"
        If bValue Then
            Reg.WriteDWORD KEY_EXPLORER, sName, 127
        Else
            Reg.KillValue KEY_EXPLORER, sName
        End If
    Case "NoMyComputer"
        If bValue Then
            Reg.WriteDWORD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\NonEnum", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 1
            Reg.WriteDWORD "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\NonEnum", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 1
        Else
            Reg.KillValue "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\NonEnum", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
            Reg.KillValue "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\NonEnum", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}"
        End If
    Case Else
        If bValue Then
            Reg.WriteDWORD KEY_EXPLORER, sName, 1
        Else
            Reg.KillValue KEY_EXPLORER, sName
        End If
    End Select

End Sub

Public Sub Split2Array()

    RegNameArr = Split(CHK_NAME, ";")
    RegCaptionArr = Split(CHK_CAPTION, ";")
    RegTipsArr = Split(CHK_TIPS, ";")

End Sub



