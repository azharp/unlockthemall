Attribute VB_Name = "modRecycleBin"
Option Explicit
Private Type SHFILEOPSTRUCT
    hwnd                                    As Long
    wFunc                                   As Long
    pFrom                                   As String
    pTo                                     As String
    fFlags                                  As Integer
    fAnyOperationsAborted                   As Boolean
    hNameMappings                           As Long
    lpszProgressTitle                       As String
End Type
Private Const FO_DELETE                 As Long = &H3
Private Const FOF_ALLOWUNDO             As Long = &H40
Private Const FOF_NOCONFIRMATION        As Long = &H10
Private Const FOF_CREATEPROGRESSDLG     As Long = &H0
Private Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long

Public Function RecycleBin(ByVal Filename As String, _
                           Optional ByVal bConfirm As Boolean = True) As Boolean

Dim FileOps  As SHFILEOPSTRUCT
Dim tmpConst As Long

    On Error GoTo ErrHand
    If bConfirm Then
        tmpConst = FOF_ALLOWUNDO Or FOF_CREATEPROGRESSDLG
    Else
        tmpConst = FOF_NOCONFIRMATION Or FOF_ALLOWUNDO Or FOF_CREATEPROGRESSDLG
    End If
    With FileOps
        .wFunc = FO_DELETE
        .pFrom = Filename
        .fFlags = tmpConst
    End With
    RecycleBin = Not SHFileOperation(FileOps)

Exit Function

ErrHand:
    RecycleBin = False

End Function


