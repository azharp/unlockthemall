Attribute VB_Name = "modData"
Option Explicit
Private Type AppVariables
    AppTitle            As String
    AppPath             As String
    AppExePath          As String
    AppVersion          As String
    AppSettings         As String
    AppHWND             As Long
    TrayHWND            As Long
    System32            As String
    Windows             As String
    StrAll              As String
    StrCurr             As String
End Type
Private Type SysVariables
    Registry            As Long
    Process             As Long
    Startup             As Long
    Window              As Long
    WindowInv           As Long
End Type
Private Type ProcessInfo
    sName               As String
    lPID                As Long
End Type
Private Type StartupInfo
    sLoc                As String
    sName               As String
    sPath               As String
End Type
Private Type WindowInfo
    sCaption            As String
    lHWND               As Long
End Type
Public AppVar       As AppVariables
Public SysVar       As SysVariables
Public ProcNow()    As ProcessInfo
Public VisNow()     As WindowInfo
Public InvNow()     As WindowInfo
Public ProcOld()    As ProcessInfo
Public VisOld()     As WindowInfo
Public InvOld()     As WindowInfo
Public mProcNow()   As ProcessInfo
Public mVisNow()    As WindowInfo
Public mInvNow()    As WindowInfo
Public mStrInfo()   As StartupInfo
Public mProcOld()   As ProcessInfo
Public mVisOld()    As WindowInfo
Public mInvOld()    As WindowInfo
Public LastDir      As String

Public Sub CopyProcType(tSrc() As ProcessInfo, _
                        tDest() As ProcessInfo)

    tDest = tSrc

End Sub

Public Sub CopyWndType(tSrc() As WindowInfo, _
                       tDest() As WindowInfo)

    tDest = tSrc

End Sub

Public Sub FillProcInfo(ByVal Index As Long, _
                        ByVal lPID As Long, _
                        ByVal sName As String)

    If bMenuMode Then
        ReDim Preserve mProcNow(Index) As ProcessInfo
        mProcNow(Index).lPID = lPID
        mProcNow(Index).sName = sName
    Else
        ReDim Preserve ProcNow(Index) As ProcessInfo
        ProcNow(Index).lPID = lPID
        ProcNow(Index).sName = sName
    End If

End Sub

Public Sub FillProcList(oList As ListBox)

Dim i As Long

    GetAllProcess
    oList.Clear
    LockWindowUpdate oList.hwnd
    For i = 0 To UBound(ProcNow)
        oList.AddItem ProcNow(i).sName
        oList.ItemData(oList.NewIndex) = ProcNow(i).lPID
        DoEvents
    Next i
    AddHScroll oList
    LockWindowUpdate 0&

End Sub

Public Sub FillStrInfo(ByVal Index As Long, _
                       ByVal sLoc As String, _
                       ByVal sName As String, _
                       ByVal sPath As String)

    ReDim Preserve mStrInfo(Index) As StartupInfo
    mStrInfo(Index).sLoc = sLoc
    mStrInfo(Index).sName = sName
    mStrInfo(Index).sPath = sPath

End Sub

Public Sub FillWndInfo(ByVal Index As Long, _
                       ByVal sCaption As String, _
                       ByVal lHWND As Long, _
                       ByVal bVis As Boolean)

    If bMenuMode Then
        If bVis Then
            ReDim Preserve mVisNow(Index) As WindowInfo
            mVisNow(Index).sCaption = sCaption
            mVisNow(Index).lHWND = lHWND
        Else
            ReDim Preserve mInvNow(Index) As WindowInfo
            mInvNow(Index).sCaption = sCaption
            mInvNow(Index).lHWND = lHWND
        End If
    Else
        If bVis Then
            ReDim Preserve VisNow(Index) As WindowInfo
            VisNow(Index).sCaption = sCaption
            VisNow(Index).lHWND = lHWND
        Else
            ReDim Preserve InvNow(Index) As WindowInfo
            InvNow(Index).sCaption = sCaption
            InvNow(Index).lHWND = lHWND
        End If
    End If

End Sub

Public Sub FillWndList(oList As ListBox, _
                       ByVal bVis As Boolean)

Dim i As Long

    GetAllWindows
    oList.Clear
    LockWindowUpdate oList.hwnd
    If bVis Then
        For i = 0 To UBound(VisNow)
            oList.AddItem VisNow(i).sCaption
            oList.ItemData(oList.NewIndex) = VisNow(i).lHWND
            DoEvents
        Next i
    Else
        For i = 0 To UBound(InvNow)
            oList.AddItem InvNow(i).sCaption
            oList.ItemData(oList.NewIndex) = InvNow(i).lHWND
            DoEvents
        Next i
    End If
    AddHScroll oList
    LockWindowUpdate 0&

End Sub

Public Function FindProcData(tName() As ProcessInfo, _
                             ByVal lPID As Long) As Boolean

Dim i As Long

    For i = 0 To UBound(tName)
        If tName(i).lPID = lPID Then
            FindProcData = True
            Exit Function
        End If
        DoEvents
    Next i
    FindProcData = False

End Function

Public Function FindWndData(tName() As WindowInfo, _
                            ByVal lHWND As Long) As Boolean

Dim i As Long

    For i = 0 To UBound(tName)
        If tName(i).lHWND = lHWND Then
            FindWndData = True
            Exit Function
        End If
        DoEvents
    Next i
    FindWndData = False

End Function

Public Function RemoveItem(oList As ListBox, _
                           ByVal sData As Long) As Boolean

Dim i As Long

    On Error Resume Next
    For i = 0 To oList.ListCount - 1
        If oList.ItemData(i) = sData Then
            oList.RemoveItem i
            RemoveItem = True
            Exit Function
        End If
        DoEvents
    Next i
    RemoveItem = False
    On Error GoTo 0

End Function


