Attribute VB_Name = "modTray"
Option Explicit
Private Type NOTIFYICONDATA
    cbSize                        As Long
    hwnd                          As Long
    uID                           As Long
    uFlags                        As Long
    uCallbackMessage              As Long
    hIcon                         As Long
    szTip                         As String * 128
    dwState                       As Long
    dwStateMask                   As Long
    szInfo                        As String * 256
    uTimeout                      As Long
    szInfoTitle                   As String * 64
    dwInfoFlags                   As Long
End Type
Private NID                   As NOTIFYICONDATA
Private Const NIM_ADD         As Long = &H0
Private Const NIM_MODIFY      As Long = &H1
Private Const NIM_DELETE      As Long = &H2
Private Const NIF_MESSAGE     As Long = &H1
Private Const NIF_ICON        As Long = &H2
Private Const NIF_TIP         As Long = &H4
Private Const NIF_INFO        As Long = &H10
Private Const NIF_DOALL       As Double = NIF_ICON Or NIF_INFO Or NIF_MESSAGE Or NIF_TIP
Private Const NIIF_INFO       As Long = &H1
Private Const WM_MOUSEMOVE    As Long = &H200
Public Const WM_RBUTTONUP     As Long = &H205
Public Const WM_LBUTTONUP     As Long = &H202
Public Const WM_MBUTTONUP     As Long = &H208
Private Declare Function Shell_NotifyIcon Lib "shell32.dll" Alias "Shell_NotifyIconA" (ByVal dwMessage As Long, _
                                                                                       lpData As NOTIFYICONDATA) As Long
Private Declare Function SetTimer Lib "user32.dll" (ByVal hwnd As Long, _
                                                    ByVal nIDEvent As Long, _
                                                    ByVal uElapse As Long, _
                                                    ByVal lpTimerFunc As Long) As Long
Private Declare Function KillTimer Lib "user32" (ByVal hwnd As Long, _
                                                 ByVal nIDEvent As Long) As Long

Public Function CreateTrayIcon() As Long

    With NID
        .cbSize = Len(NID)
        .hwnd = AppVar.TrayHWND
        .uID = vbNull
        .uFlags = NIF_DOALL
        .uCallbackMessage = WM_MOUSEMOVE
        .hIcon = formMain.Icon
        .szTip = AppVar.AppTitle & " v" & AppVar.AppVersion & vbNullChar
    End With
    CreateTrayIcon = Shell_NotifyIcon(NIM_ADD, NID)

End Function

Public Function DeleteTrayIcon() As Long

    With NID
        .cbSize = Len(NID)
        .hwnd = AppVar.TrayHWND
        .uID = vbNull
    End With
    DeleteTrayIcon = Shell_NotifyIcon(NIM_DELETE, NID)

End Function

Public Sub KillBaloon(ByVal lngHWnd As Long, _
                      ByVal uMsg As Long, _
                      ByVal idEvent As Long, _
                      ByVal dwTime As Long)


    NID.szInfo = vbNullString & vbNullChar
    NID.szInfoTitle = vbNullString & vbNullChar
    Shell_NotifyIcon NIM_MODIFY, NID
    KillTimer AppVar.TrayHWND, 99

End Sub

Public Function ShowTrayBalloon() As Long

    GetValue 1
    GetAllProcess
    GetAllStartup 1
    GetAllWindows
    With NID
        .cbSize = Len(NID)
        .hwnd = AppVar.TrayHWND
        .uID = vbNull
        .uFlags = NIF_DOALL
        .uCallbackMessage = WM_MOUSEMOVE
        .hIcon = formMain.Icon
        .szTip = AppVar.AppTitle & " v" & AppVar.AppVersion & vbNullChar
        .dwState = 0
        .dwStateMask = 0
        .szInfo = "STATUS :" & vbNewLine & _
                  "- " & SysVar.Registry & " registry settings changed" & vbNewLine & _
                  "- " & SysVar.Process & " processes currently running" & vbNewLine & _
                  "- " & SysVar.Startup & " startup items detected" & vbNewLine & _
                  "- " & SysVar.Window & " visible windows detected" & vbNewLine & _
                  "- " & SysVar.WindowInv & " invisible windows detected" & vbNullChar
        .szInfoTitle = AppVar.AppTitle & " v" & AppVar.AppVersion & vbNullChar
        .dwInfoFlags = NIIF_INFO
        .uTimeout = 1000
    End With
    ShowTrayBalloon = Shell_NotifyIcon(NIM_MODIFY, NID)
    SetTimer AppVar.TrayHWND, 99, 5000, AddressOf KillBaloon

End Function


