Attribute VB_Name = "modINIFile"
Option Explicit
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
                                                                                                      ByVal lpKeyName As String, _
                                                                                                      ByVal lpString As Any, _
                                                                                                      ByVal lpFilePath As String) As Long
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
                                                                                                  ByVal lpKeyName As String, _
                                                                                                  ByVal lpDefault As String, _
                                                                                                  ByVal lpReturnedString As String, _
                                                                                                  ByVal nSize As Long, _
                                                                                                  ByVal lpFilePath As String) As Long

Public Function ReadINI(ByVal SecName As String, _
                        ByVal KeyName As String) As String

Dim lRet     As Long
Dim KeyValue As String

    KeyValue = String$(255, 0)
    lRet = GetPrivateProfileString(SecName, KeyName, vbNullString, KeyValue, Len(KeyValue), AppVar.AppSettings)
    If lRet = 0 Then
        ReadINI = vbNullString
    Else
        ReadINI = Trim$(Left$(KeyValue, lRet))
    End If

End Function

Public Function WriteINI(ByVal SecName As String, _
                         ByVal KeyName As String, _
                         Optional ByVal KeyValue As String = vbNullString) As Long

    WriteINI = WritePrivateProfileString(SecName, KeyName, KeyValue, AppVar.AppSettings)

End Function


