Attribute VB_Name = "modDeclare"
Option Explicit
Public sCurrMode               As Single
Public bLoaded                 As Boolean
Public Type SHITEMID
    cb                             As Long
    abID                           As Byte
End Type
Public Type ITEMIDLIST
    mkid                           As SHITEMID
End Type
Public Const SWP_NOMOVE        As Long = 2
Public Const SWP_NOSIZE        As Long = 1
Public Const HWND_TOPMOST      As Long = -1
Public Const HWND_NOTOPMOST    As Long = -2
Public Const NOERROR           As Long = 0
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Function InitCommonControls Lib "comctl32.dll" () As Long
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, _
                                                                              ByVal lpOperation As String, _
                                                                              ByVal lpFile As String, _
                                                                              ByVal lpParameters As String, _
                                                                              ByVal lpDirectory As String, _
                                                                              ByVal nShowCmd As Long) As Long
Public Declare Function SHGetSpecialFolderLocation Lib "shell32.dll" (ByVal hwndOwner As Long, _
                                                                      ByVal nFolder As Long, _
                                                                      pidl As ITEMIDLIST) As Long
Public Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias "SHGetPathFromIDListA" (ByVal pidl As Long, _
                                                                                            ByVal pszPath As String) As Long
Public Declare Function LockWindowUpdate Lib "user32.dll" (ByVal hwndLock As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, _
                                                   ByVal hWndInsertAfter As Long, _
                                                   ByVal X As Long, _
                                                   ByVal Y As Long, _
                                                   ByVal cx As Long, _
                                                   ByVal cy As Long, _
                                                   ByVal wFlags As Long) As Long
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, _
                                                                       ByVal wMsg As Long, _
                                                                       ByVal wParam As Long, _
                                                                       lParam As Any) As Long
Public Declare Function PathFileExists Lib "shlwapi.dll" Alias "PathFileExistsA" (ByVal pszPath As String) As Long
Public Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" (ByVal lpExistingFileName As String, _
                                                                   ByVal lpNewFileName As String) As Long



