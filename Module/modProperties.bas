Attribute VB_Name = "modProperties"
Option Explicit
Private Type SHELLEXECUTEINFO
    cbSize                                    As Long
    fMask                                     As Long
    hwnd                                      As Long
    lpVerb                                    As String
    lpFile                                    As String
    lpParameters                              As String
    lpDirectory                               As String
    nShow                                     As Long
    hInstApp                                  As Long
    lpIDList                                  As Long
    lpClass                                   As String
    hkeyClass                                 As Long
    dwHotKey                                  As Long
    hIcon                                     As Long
    hProcess                                  As Long
End Type
Private iLink                             As ShellLinkObject
Private Const SEE_MASK_INVOKEIDLIST       As Long = &HC
Private Const SEE_MASK_NOCLOSEPROCESS     As Long = &H40
Private Const SEE_MASK_FLAG_NO_UI         As Long = &H400
Private Declare Function ShellExecuteEX Lib "shell32.dll" Alias "ShellExecuteEx" (SEI As SHELLEXECUTEINFO) As Long

Public Function GetLinkInfo(ByVal Path As String) As String

Dim iShell  As Shell
Dim iFolder As Folder
Dim ifItem  As FolderItem

    On Error GoTo ErrHand
    If IsFileExist(Path) Then
        Set iShell = New Shell
        Set iFolder = iShell.NameSpace(Left$(Path, InStrRev(Path, "\", , 1) - 1))
        Set ifItem = iFolder.ParseName(GetFileName(Path))
        Set iLink = ifItem.GetLink
        GetLinkInfo = iLink.Path
        Set iShell = Nothing
        Set iFolder = Nothing
        Set ifItem = Nothing
    End If

Exit Function

ErrHand:
    GetLinkInfo = 0
    Set iShell = Nothing
    Set iFolder = Nothing
    Set ifItem = Nothing

End Function

Public Sub ShowProperties(ByVal Filename As String, _
                          ByVal OwnerhWnd As Long)

Dim SEI As SHELLEXECUTEINFO

    With SEI
        .cbSize = Len(SEI)
        .fMask = SEE_MASK_NOCLOSEPROCESS Or SEE_MASK_INVOKEIDLIST Or SEE_MASK_FLAG_NO_UI
        .hwnd = OwnerhWnd
        .lpVerb = "Properties"
        .lpFile = Filename
        .lpParameters = vbNullChar
        .lpDirectory = vbNullChar
        .nShow = 0
        .hInstApp = 0
        .lpIDList = 0
    End With
    ShellExecuteEX SEI

End Sub


