Attribute VB_Name = "modDialog"
Option Explicit
Private Type OPENFILENAME
    lStructSize         As Long
    hwndOwner           As Long
    hInstance           As Long
    lpstrFilter         As String
    lpstrCustomFilter   As String
    nMaxCustFilter      As Long
    nFilterIndex        As Long
    lpstrFile           As String
    nMaxFile            As Long
    lpstrFileTitle      As String
    nMaxFileTitle       As Long
    lpstrInitialDir     As String
    lpstrTitle          As String
    flags               As Long
    nFileOffset         As Integer
    nFileExtension      As Integer
    lpstrDefExt         As String
    lCustData           As Long
    lpfnHook            As Long
    lpTemplateName      As String
End Type
Private Declare Function GetOpenFileName Lib "comdlg32.dll" Alias "GetOpenFileNameA" (pOpenfilename As OPENFILENAME) As Long

Public Function ShowOpenDialog() As String

Dim sPath  As String
Dim OFName As OPENFILENAME
Dim sFile  As String

    If Len(LastDir) > 0 Then
        sPath = LastDir
    Else
        sPath = AppVar.AppPath
    End If
    With OFName
        .lStructSize = Len(OFName)
        .hwndOwner = AppVar.AppHWND
        .hInstance = App.hInstance
        .lpstrFilter = "Application (*.exe)" & vbNullChar & "*.exe" & vbNullChar & "All Files (*.*)" & vbNullChar & "*.*" & vbNullChar
        .lpstrFile = Space$(254)
        .nMaxFile = 255
        .lpstrFileTitle = Space$(254)
        .nMaxFileTitle = 255
        .lpstrInitialDir = sPath
        .lpstrTitle = "Please select file..."
        .flags = 0
    End With
    If GetOpenFileName(OFName) Then
        sFile = Trim$(OFName.lpstrFile)
        LastDir = GetFilePath(sFile)
        ShowOpenDialog = sFile
    Else
        ShowOpenDialog = vbNullString
    End If

End Function

