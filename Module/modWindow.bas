Attribute VB_Name = "modWindow"
Option Explicit
Private Type Rect
    Left                             As Long
    Top                              As Long
    Right                            As Long
    Bottom                           As Long
End Type
Public Const SMTO_BLOCK          As Long = &H1
Public Const SMTO_ABORTIFHUNG    As Long = &H2
Public Const WM_NULL             As Long = &H0
Public Const SW_NORMAL           As Integer = 1
Public Const SW_RESTORE          As Integer = 9
Public Const SW_MAXIMIZE         As Integer = 3
Public Const SW_MINIMIZE         As Integer = 6
Public Const SW_HIDE             As Integer = 0
Public Const SW_SHOW             As Integer = 5
Private Const WS_EX_LAYERED      As Long = &H80000
Private Const RDW_ALLCHILDREN    As Long = &H80
Private Const RDW_ERASE          As Long = &H4
Private Const RDW_FRAME          As Long = &H400
Private Const RDW_INVALIDATE     As Long = &H1
Private Const GW_CHILD           As Integer = 5
Private Const GW_HWNDFIRST       As Integer = 0
Private Const GW_HWNDNEXT        As Integer = 2
Private Const GWL_EXSTYLE        As Long = (-20)
Private Const LWA_ALPHA          As Long = &H2
Private Declare Function GetWindow Lib "user32" (ByVal hwnd As Long, _
                                                 ByVal wCmd As Long) As Long
Public Declare Function GetForegroundWindow Lib "user32" () As Long
Public Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Long, _
                                                                            ByVal nIndex As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, _
                                                                     ByVal lpWindowName As String) As Long
Public Declare Function ShowWindow Lib "user32" (ByVal hwnd As Long, _
                                                 ByVal nCmdShow As Long) As Long
Public Declare Function SetWindowText Lib "user32.dll" Alias "SetWindowTextA" (ByVal hwnd As Long, _
                                                                               ByVal lpString As String) As Long
Public Declare Function SendMessageTimeout Lib "user32" Alias "SendMessageTimeoutA" (ByVal hwnd As Long, _
                                                                                     ByVal msg As Long, _
                                                                                     ByVal wParam As Long, _
                                                                                     ByVal lParam As Long, _
                                                                                     ByVal fuFlags As Long, _
                                                                                     ByVal uTimeout As Long, _
                                                                                     pdwResult As Long) As Long
Public Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As Long, _
                                                               lpdwProcessId As Long) As Long
Private Declare Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As Long, _
                                                       ByVal lParam As Long) As Long
Private Declare Function GetWindowText Lib "user32.dll" Alias "GetWindowTextA" (ByVal hwnd As Long, _
                                                                                ByVal lpString As String, _
                                                                                ByVal nMaxCount As Long) As Long
Private Declare Function GetWindowTextLength Lib "user32.dll" Alias "GetWindowTextLengthA" (ByVal hwnd As Long) As Long
Private Declare Function EnableWindow Lib "user32.dll" (ByVal hwnd As Long, _
                                                        ByVal fEnable As Long) As Long
Private Declare Function IsWindowEnabled Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function FindWindowEx Lib "user32.dll" Alias "FindWindowExA" (ByVal hWndParent As Long, _
                                                                              ByVal hwndChildAfter As Long, _
                                                                              ByVal lpszClass As String, _
                                                                              ByVal lpszWindow As String) As Long
Private Declare Function IsWindowVisible Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, _
                                                                            ByVal nIndex As Long, _
                                                                            ByVal dwNewLong As Long) As Long
Private Declare Function RedrawWindow Lib "user32" (ByVal hwnd As Long, _
                                                    lprcUpdate As Rect, _
                                                    ByVal hrgnUpdate As Long, _
                                                    ByVal fuRedraw As Long) As Long
Private Declare Function SetLayeredWindowAttributes Lib "user32" (ByVal hwnd As Long, _
                                                                  ByVal crKey As Long, _
                                                                  ByVal bAlpha As Byte, _
                                                                  ByVal dwFlags As Long) As Long

Public Sub DisableWinFeature(ByRef Mode As Single, _
                             Optional ByRef Item As Integer, _
                             Optional ByRef Status As Single)

Dim lTaskbar As Long
Dim lStart   As Long
Dim lSysTray As Long
Dim lClock   As Long
Dim lLaunch  As Long

    lTaskbar = FindWindow("Shell_TrayWnd", vbNullString)
    lStart = FindWindowEx(lTaskbar, 0&, "Button", vbNullString)
    lSysTray = FindWindowEx(lTaskbar, 0&, "TrayNotifywnd", vbNullString)
    lClock = FindWindowEx(lSysTray, 0&, "TrayClockWClass", vbNullString)
    lLaunch = FindWindowEx(FindWindowEx(lTaskbar, 0&, "ReBarWindow32", vbNullString), 0&, "ToolBarWindow32", vbNullString)
    Select Case Mode
    Case 1
        With formMain
            .menuDisArr(0).Checked = lStart And IsWindowEnabled(lStart) = 0
            .menuDisArr(1).Checked = lSysTray And IsWindowEnabled(lSysTray) = 0
            .menuDisArr(2).Checked = lTaskbar And IsWindowEnabled(lTaskbar) = 0
            .menuDisArr(3).Checked = lClock And IsWindowEnabled(lClock) = 0
            .menuDisArr(4).Checked = lLaunch And IsWindowEnabled(lLaunch) = 0
        End With
    Case 2
        Select Case Item
        Case 0
            EnableWindow lStart, Status
        Case 1
            EnableWindow lSysTray, Status
        Case 2
            EnableWindow lTaskbar, Status
        Case 3
            EnableWindow lClock, Status
        Case 4
            EnableWindow lLaunch, Status
        End Select
    End Select

End Sub

Private Function EnumAllWindows(ByVal lngHWnd As Long, _
                                ByVal lParam As Long) As Long


Dim txtLen   As Long
Dim sCaption As String

    txtLen = GetWindowTextLength(lngHWnd) + 1
    sCaption = Space$(txtLen)
    GetWindowText lngHWnd, sCaption, txtLen
    sCaption = Left$(sCaption, Len(sCaption) - 1)
    If Len(sCaption) > 0 Or (formMain.chkFilter.Value = 0 And formMain.Visible) Then
        If LenB(sCaption) = 0 Then
            If formMain.chkFilter.Value = 0 Then
                sCaption = "N/A"
            End If
        End If
        If IsWindowVisible(lngHWnd) > 0 Then
            If sCaption = formMain.Caption Then
                GoTo Finish
            End If
            FillWndInfo SysVar.Window, sCaption, lngHWnd, True
            SysVar.Window = SysVar.Window + 1
        End If
        If IsWindowVisible(lngHWnd) = 0 Then
            If sCaption = formMain.Caption Then
                GoTo Finish
            End If
            FillWndInfo SysVar.WindowInv, sCaption, lngHWnd, False
            SysVar.WindowInv = SysVar.WindowInv + 1
        End If
    End If
    DoEvents
Finish:
    EnumAllWindows = 1

End Function

Public Sub GetAllWindows()

    If bMenuMode Then
        Erase mVisNow
        Erase mInvNow
    Else
        Erase VisNow
        Erase InvNow
    End If
    SysVar.Window = 0
    SysVar.WindowInv = 0
    EnumWindows AddressOf EnumAllWindows, 0

End Sub

Public Function GetWndStyle(ByVal lngHWnd As Long) As Boolean

Dim lRet As Long

    lRet = GetWindowLong(lngHWnd, GWL_EXSTYLE)
    GetWndStyle = (lRet And &H8&) = &H8&

End Function

Public Sub ReleaseDisplay(ByVal lngHWnd As Long)

    SetLayered lngHWnd, False, 255

End Sub

Public Sub SetAllChild(lHWND As Long, _
                       Mode As Integer)

Dim hChild As Long

    hChild = GetWindow(lHWND, GW_CHILD Or GW_HWNDFIRST)
    Do While hChild > 0
        If Mode = 0 Then
            ShowWindow hChild, SW_SHOW
        ElseIf Mode = 1 Then
            ShowWindow hChild, SW_HIDE
        ElseIf Mode = 3 Then
            EnableWindow hChild, 1
        ElseIf Mode = 4 Then
            EnableWindow hChild, 0
        Else
            Exit Sub
        End If
        SetAllChild hChild, Mode
        hChild = GetWindow(hChild, GW_HWNDNEXT)
    Loop

End Sub

Public Sub SetLayered(ByVal lngHWnd As Long, _
                      ByVal bolSetAs As Boolean, _
                      ByVal bAlpha As Byte)

Dim nullRect As Rect
Dim lRet     As Long

    lRet = GetWindowLong(lngHWnd, GWL_EXSTYLE)
    If bolSetAs Then
        lRet = lRet Or WS_EX_LAYERED
    Else
        lRet = lRet And Not WS_EX_LAYERED
    End If
    SetWindowLong lngHWnd, GWL_EXSTYLE, lRet
    If bolSetAs Then
        SetLayeredWindowAttributes lngHWnd, 0, bAlpha, LWA_ALPHA
    Else
        RedrawWindow lngHWnd, nullRect, 0&, RDW_ALLCHILDREN Or RDW_ERASE Or RDW_FRAME Or RDW_INVALIDATE
    End If

End Sub


