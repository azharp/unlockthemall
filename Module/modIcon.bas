Attribute VB_Name = "modIcon"
Option Explicit
Private Const DI_MASK     As Long = &H1
Private Const DI_IMAGE    As Long = &H2
Private Const DI_NORMAL   As Double = DI_MASK Or DI_IMAGE
Private Declare Function ExtractAssociatedIcon Lib "shell32.dll" Alias "ExtractAssociatedIconA" (ByVal hInst As Long, _
                                                                                                 ByVal lpIconPath As String, _
                                                                                                 lpiIcon As Long) As Long
Private Declare Function DrawIconEx Lib "user32" (ByVal hDC As Long, _
                                                  ByVal xLeft As Long, _
                                                  ByVal yTop As Long, _
                                                  ByVal hIcon As Long, _
                                                  ByVal cxWidth As Long, _
                                                  ByVal cyWidth As Long, _
                                                  ByVal istepIfAniCur As Long, _
                                                  ByVal hbrFlickerFreeDraw As Long, _
                                                  ByVal diFlags As Long) As Long
Private Declare Function DestroyIcon Lib "user32" (ByVal hIcon As Long) As Long

Public Function GetFileIcon(FILEPATH As String, _
                            pDisp As PictureBox) As Long

Dim mIcon As Long

    If IsFileExist(FILEPATH) Then
        pDisp.Cls
        mIcon = ExtractAssociatedIcon(App.hInstance, FILEPATH, 0)
        DrawIconEx pDisp.hDC, 0, 0, mIcon, 0, 0, 0, 0, DI_NORMAL
        DestroyIcon mIcon
        GetFileIcon = mIcon
    Else
        GetFileIcon = 0
    End If

End Function


