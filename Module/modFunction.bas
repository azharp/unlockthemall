Attribute VB_Name = "modFunction"
Option Explicit

Public Sub AlwaysOnTop(ByVal lHandle As Long, _
                       ByVal bState As Boolean)

Dim tmpConst As Long

    If bState Then
        tmpConst = HWND_TOPMOST
    Else
        tmpConst = HWND_NOTOPMOST
    End If
    SetWindowPos lHandle, tmpConst, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE

End Sub

Public Sub CenterForm(ByVal oForm As Form)

Dim WndX As Long
Dim WndY As Long

    WndX = Val(ReadINI(INI_MAIN, "WindowX"))
    WndY = Val(ReadINI(INI_MAIN, "WindowY"))
    If (WndX > 0 And WndX < Screen.Width - oForm.Width) And (WndY > 0 And WndY < Screen.Height - oForm.Height) Then
        oForm.Top = WndY
        oForm.Left = WndX
    Else
        oForm.Top = (Screen.Height - oForm.Height) / 2
        oForm.Left = (Screen.Width - oForm.Width) / 2
    End If

End Sub

Public Sub ChangeMenuState(ByVal bStatus As Boolean)

    With formMain
        .menuTrayReg.Visible = bStatus
        .menuTrayProc.Visible = bStatus
        .menuTrayStr.Visible = bStatus
        .menuTrayWnd.Visible = bStatus
        .menuTrayQuick.Visible = Not bStatus
    End With

End Sub

Public Function CheckInstance(frmIn As Form) As Boolean

Dim sTitle  As String
Dim lHandle As Long

    CheckInstance = False
    With frmIn
        .Hide
        sTitle = .Caption
        .Caption = sTitle & " - New"
    End With
    lHandle = FindWindow("ThunderRT6FormDC", sTitle)
    frmIn.Caption = sTitle
    If lHandle Then
        ReleaseDisplay lHandle
        ShowWindow lHandle, SW_SHOW And SW_NORMAL
        SetForegroundWindow lHandle
        CloseHandle lHandle
        CheckInstance = True
        End
    Else
        CloseHandle lHandle
    End If

End Function

Public Sub ErrorHandler(ByVal sError As ErrObject, _
                        ByVal ProcedureName As String)

    Open AppVar.AppPath & "\UTAError.log" For Append As #1
    Print #1, Now & "::" & ProcedureName & "::" & sError.Number & "::" & sError.Description
    Close #1

End Sub

Public Sub GetAllAppVariables()

    With AppVar
        .AppTitle = App.Title
        .AppPath = App.Path
        .AppExePath = """" & StripSlash(App.Path & "\" & App.EXEName & ".exe") & """"
        .AppVersion = App.Major & "." & App.Minor & "." & App.Revision
        .AppHWND = formMain.hwnd
        .AppSettings = StripSlash(App.Path & "\UTASettings.ini")
        .TrayHWND = formMain.picTray.hwnd
        .System32 = GetSpecialfolder(&H25)
        .Windows = GetSpecialfolder(&H24)
        .StrAll = GetSpecialfolder(&H18)
        .StrCurr = GetSpecialfolder(&H7)
    End With

End Sub

Public Function GetBaseName(ByVal sPath As String) As String

Dim DotPos   As Long
Dim SlashPos As Long

    DotPos = InStrRev(sPath, ".", -1, vbBinaryCompare)
    SlashPos = InStrRev(sPath, "\", -1, vbBinaryCompare)
    If SlashPos < DotPos Then
        GetBaseName = Mid$(sPath, SlashPos + 1, (DotPos - 1) - SlashPos)
    Else
        GetBaseName = sPath
    End If

End Function

Public Function GetFileName(ByVal sPath As String) As String

Dim lPos As Long

    lPos = InStrRev(sPath, "\", -1, vbBinaryCompare)
    If lPos > 0 Then
        GetFileName = Mid$(sPath, lPos + 1)
    Else
        GetFileName = sPath
    End If

End Function

Public Function GetFilePath(ByVal sPath As String) As String

Dim lPos As Long

    lPos = InStrRev(sPath, "\", -1, vbBinaryCompare)
    If lPos > 0 Then
        GetFilePath = Left$(sPath, lPos - 1)
    Else
        GetFilePath = vbNullString
    End If

End Function

Public Function GetSpecialfolder(CSIDL As Long) As String

Dim Path As String
Dim R    As Long
Dim IDL  As ITEMIDLIST

    R = SHGetSpecialFolderLocation(100, CSIDL, IDL)
    If R = NOERROR Then
        Path = Space$(512)
        R = SHGetPathFromIDList(ByVal IDL.mkid.cb, ByVal Path)
        GetSpecialfolder = Left$(Path, InStr(Path, vbNullChar) - 1)
    Else
        GetSpecialfolder = vbNullString
    End If

End Function

Public Function IsFileExist(Path As String) As Boolean

    On Error GoTo Hell
    IsFileExist = PathFileExists(Path)

Exit Function

Hell:
    IsFileExist = False

End Function

Public Sub MenuMainState(ByVal Reg As Boolean, _
                         ByVal PROC As Boolean, _
                         ByVal Startup As Boolean, _
                         ByVal Wnd As Boolean)

    With formMain
        .frmCont(0).Visible = Reg
        .frmCont(1).Visible = PROC
        .frmCont(2).Visible = Startup
        .frmCont(3).Visible = Wnd
        .cmdApply.Enabled = Reg
        .menuMainArr(0).Checked = Reg
        .menuMainArr(1).Checked = PROC
        .menuMainArr(2).Checked = Startup
        .menuMainArr(3).Checked = Wnd
    End With

End Sub

Public Sub MenuStrState(ByVal bState As Boolean)

    With formMain
        .menuStrDelete.Enabled = bState
        .menuStrLoc.Enabled = bState
        .menuStrMove.Enabled = bState
        .menuStrProperties.Enabled = bState
        .menuStrStart.Enabled = bState
    End With

End Sub

Public Function ParseString(ByVal Path As String) As String


Dim tmpStr As String
Dim Check1 As Long
Dim Check2 As Long
Dim Check3 As Long
Dim Ext1   As Long
Dim Ext2   As Long

    Path = Replace$(Path, """", vbNullString)
    If Len(Trim$(Path)) > 0 And IsFileExist(Path) Then
        tmpStr = Path
    Else
        If IsFileExist(Path) Then
            tmpStr = Path
        Else
            Check1 = InStr(1, Path, " -")
            Check2 = InStr(1, Path, " /")
            Check3 = InStr(1, Path, ":\")
            Ext1 = InStr(1, Path, ".exe")
            Ext2 = InStr(1, Path, ".dll")
            If Check1 Xor Check2 Then
                If Check1 Then
                    tmpStr = Left$(Path, Check1 - 1)
                Else
                    tmpStr = Left$(Path, Check2 - 1)
                End If
            ElseIf Check3 And Mid$(Path, 2, 2) <> ":\" Then
                Path = Mid$(Path, Check3 - 1)
                tmpStr = Left$(Path, InStr(1, Path, "dll,") + 2)
            ElseIf Ext1 Xor Ext2 Then
                If Ext1 = 0 Then
                    tmpStr = Mid$(Path, 1, Ext2 + 4)
                ElseIf Ext2 = 0 Then
                    tmpStr = Mid$(Path, 1, Ext1 + 4)
                ElseIf Ext1 <= Ext2 Then
                    tmpStr = Mid$(Path, 1, Ext1 + 4)
                ElseIf Ext1 >= Ext2 Then
                    tmpStr = Mid$(Path, 1, Ext2 + 4)
                End If
            Else
                tmpStr = Path
            End If
            If Not InStr(1, tmpStr, ":\") Then
                tmpStr = Replace$(tmpStr, "%SystemRoot%", AppVar.Windows)
                If IsFileExist(AppVar.System32 & "\" & tmpStr) Then
                    tmpStr = AppVar.System32 & "\" & tmpStr
                ElseIf IsFileExist(AppVar.Windows & "\" & tmpStr) Then
                    tmpStr = AppVar.Windows & "\" & tmpStr
                End If
            End If
        End If
    End If
    ParseString = tmpStr

End Function

Private Function StripSlash(ByVal sPath As String) As String

    StripSlash = Replace$(sPath, "\\", "\")

End Function


