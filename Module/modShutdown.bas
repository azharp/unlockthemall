Attribute VB_Name = "modShutdown"
Option Explicit
Private Const EWX_LOGOFF      As Long = 0
Private Const EWX_SHUTDOWN    As Long = 1
Private Const EWX_REBOOT      As Long = 2
Private Const EWX_FORCE       As Long = 4
Private Declare Function ExitWindowsEx Lib "user32" (ByVal uFlags As Long, _
                                                     ByVal dwReserved As Long) As Long
Private Declare Function SetSystemPowerState Lib "kernel32" (ByVal fSuspend As Long, _
                                                             ByVal fForce As Long) As Long

Public Sub ShutdownSystem(ByVal intAction As Long)

Dim lngFlags As Long
Dim lngRet   As Long

    RaisePrivileges 1
    Select Case intAction
    Case 0
        lngFlags = EWX_LOGOFF
    Case 1
        lngFlags = EWX_SHUTDOWN
    Case 2
        lngFlags = EWX_REBOOT
    Case 3
        SetSystemPowerState True, True
        Exit Sub
    Case 4
        SetSystemPowerState False, True
        Exit Sub
    Case Else
        Exit Sub
    End Select
    lngRet = ExitWindowsEx(lngFlags, &HFFFF)
    If lngRet = 0 Then
        lngFlags = lngFlags Or EWX_FORCE
        lngRet = ExitWindowsEx(lngFlags, 0&)
        If lngRet = 0 Then
            lngFlags = EWX_SHUTDOWN Or EWX_FORCE
            lngRet = ExitWindowsEx(lngFlags, 0&)
        End If
    End If

End Sub


