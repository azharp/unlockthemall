Attribute VB_Name = "modProcess"
Option Explicit
Private Thread()                            As THREADENTRY32
Private Type THREADENTRY32
    dwSize                                      As Long
    cntUsage                                    As Long
    th32ThreadID                                As Long
    th32OwnerProcessID                          As Long
    tpBasePri                                   As Long
    tpDeltaPri                                  As Long
    dwFlags                                     As Long
End Type
Private Const THREAD_SUSPEND_RESUME         As Long = &H2
Private Const SYNCHRONIZE                   As Long = &H100000
Private Const PROCESS_TERMINATE             As Long = &H1
Private Const PROCESS_QUERY_INFORMATION     As Long = &H400
Private Const PROCESS_VM_READ               As Long = &H10
Private Const TH32CS_SNAPHEAPLIST           As Long = &H1
Private Const TH32CS_SNAPPROCESS            As Long = &H2
Private Const TH32CS_SNAPTHREAD             As Long = &H4
Private Const TH32CS_SNAPMODULE             As Long = &H8
Private Const TH32CS_SNAPALL                As Double = TH32CS_SNAPHEAPLIST Or TH32CS_SNAPPROCESS Or TH32CS_SNAPTHREAD Or TH32CS_SNAPMODULE
Private Const MAX_PATH                      As Long = 260
Private Const SE_PRIVILEGE_ENABLED          As Long = &H2
Private Const SE_DEBUG_NAME                 As String = "SeDebugPrivilege"
Private Const SE_SHUTDOWN_NAME              As String = "SeShutdownPrivilege"
Private Const STANDARD_RIGHTS_REQUIRED      As Long = &HF0000
Private Const TOKEN_ASSIGN_PRIMARY          As Long = &H1
Private Const TOKEN_DUPLICATE               As Long = &H2
Private Const TOKEN_IMPERSONATE             As Long = &H4
Private Const TOKEN_QUERY                   As Long = &H8
Private Const TOKEN_QUERY_SOURCE            As Long = &H10
Private Const TOKEN_ADJUST_PRIVILEGES       As Long = &H20
Private Const TOKEN_ADJUST_GROUPS           As Long = &H40
Private Const TOKEN_ADJUST_DEFAULT          As Long = &H80
Private Const TOKEN_ALL_ACCESS              As Long = STANDARD_RIGHTS_REQUIRED Or TOKEN_ASSIGN_PRIMARY Or TOKEN_DUPLICATE Or TOKEN_IMPERSONATE Or TOKEN_QUERY Or TOKEN_QUERY_SOURCE Or TOKEN_ADJUST_PRIVILEGES Or TOKEN_ADJUST_GROUPS Or TOKEN_ADJUST_DEFAULT
Private Type PROCESSENTRY32
    dwSize                                      As Long
    cntUsage                                    As Long
    th32ProcessID                               As Long
    th32DefaultHeapID                           As Long
    th32ModuleID                                As Long
    cntThreads                                  As Long
    th32ParentProcessID                         As Long
    pcPriClassBase                              As Long
    dwFlags                                     As Long
    szExeFile                                   As String * MAX_PATH
End Type
Public Type LUID
    lowpart                                     As Long
    highpart                                    As Long
End Type
Public Type LUID_AND_ATTRIBUTES
    pLuid                                       As LUID
    Attributes                                  As Long
End Type
Public Type TOKEN_PRIVILEGES
    PrivilegeCount                              As Long
    TheLuid                                     As LUID
    Attributes                                  As Long
End Type
Public Type SECURITY_ATTRIBUTES
    nLength                                     As Long
    lpSecurityDescriptor                        As Long
    bInheritHandle                              As Long
End Type
Public Declare Function CloseHandle Lib "kernel32.dll" (ByVal hObject As Long) As Long
Private Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDesiredAccess As Long, _
                                                         ByVal bInheritHandle As Long, _
                                                         ByVal dwProcessId As Long) As Long
Private Declare Function GetModuleFileNameExA Lib "psapi.dll" (ByVal hProcess As Long, _
                                                               ByVal hModule As Long, _
                                                               ByVal ModuleName As String, _
                                                               ByVal nSize As Long) As Long
Private Declare Function EnumProcessModules Lib "psapi.dll" (ByVal hProcess As Long, _
                                                             ByRef lphModule As Long, _
                                                             ByVal cb As Long, _
                                                             ByRef cbNeeded As Long) As Long
Private Declare Function CreateToolhelp32Snapshot Lib "kernel32" (ByVal lFlags As Long, _
                                                                  ByVal lProcessID As Long) As Long
Private Declare Function Process32First Lib "kernel32" (ByVal hSnapShot As Long, _
                                                        uProcess As PROCESSENTRY32) As Long
Private Declare Function Process32Next Lib "kernel32" (ByVal hSnapShot As Long, _
                                                       uProcess As PROCESSENTRY32) As Long
Private Declare Function TerminateProcess Lib "kernel32" (ByVal hProcess As Long, _
                                                          ByVal uExitCode As Long) As Long
Private Declare Function Thread32First Lib "kernel32.dll" (ByVal hSnapShot As Long, _
                                                           ByRef lpte As THREADENTRY32) As Boolean
Private Declare Function Thread32Next Lib "kernel32.dll" (ByVal hSnapShot As Long, _
                                                          ByRef lpte As THREADENTRY32) As Boolean
Private Declare Function OpenThread Lib "kernel32.dll" (ByVal dwDesiredAccess As Long, _
                                                        ByVal bInheritHandle As Boolean, _
                                                        ByVal dwThreadId As Long) As Long
Private Declare Function ResumeThread Lib "kernel32.dll" (ByVal hThread As Long) As Long
Private Declare Function SuspendThread Lib "kernel32.dll" (ByVal hThread As Long) As Long
Private Declare Function LookupPrivilegeValue Lib "advapi32.dll" Alias "LookupPrivilegeValueA" (ByVal lpSystemName As String, _
                                                                                                ByVal lpName As String, _
                                                                                                lpLuid As LUID) As Long
Private Declare Function AdjustTokenPrivileges Lib "advapi32.dll" (ByVal TokenHandle As Long, _
                                                                   ByVal DisableAllPrivileges As Long, _
                                                                   ByRef NewState As TOKEN_PRIVILEGES, _
                                                                   ByVal BufferLength As Long, _
                                                                   ByRef PreviousState As TOKEN_PRIVILEGES, _
                                                                   ByRef ReturnLength As Long) As Long
Private Declare Function OpenProcessToken Lib "advapi32.dll" (ByVal ProcessHandle As Long, _
                                                              ByVal DesiredAccess As Long, _
                                                              ByRef TokenHandle As Long) As Long
Private Declare Function GetCurrentProcess Lib "kernel32.dll" () As Long

Public Sub GetAllProcess()

Dim hSnapShot As Long
Dim uProcess  As PROCESSENTRY32
Dim pNext     As Long

    If bMenuMode Then
        Erase mProcNow
    Else
        Erase ProcNow
    End If
    SysVar.Process = 0
    hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0&)
    uProcess.dwSize = Len(uProcess)
    pNext = Process32First(hSnapShot, uProcess)
    Do While pNext
        FillProcInfo SysVar.Process, uProcess.th32ProcessID, Left$(uProcess.szExeFile, IIf(InStr(1, uProcess.szExeFile, vbNullChar) > 0, InStr(1, uProcess.szExeFile, vbNullChar) - 1, 0))
        pNext = Process32Next(hSnapShot, uProcess)
        SysVar.Process = SysVar.Process + 1
        DoEvents
    Loop
    CloseHandle hSnapShot

End Sub

Public Function GetProcAddress(ByVal lPID As String, _
                               Optional ByVal pName As String = vbNullString) As String

Dim cbNeeded          As Long
Dim Modules(1 To 200) As Long
Dim lRet              As Long
Dim ModuleName        As String
Dim hProcess          As Long
Dim sPath             As String

    hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, lPID)
    If hProcess Then
        lRet = EnumProcessModules(hProcess, Modules(1), 200, cbNeeded)
        If lRet Then
            ModuleName = Space$(MAX_PATH)
            lRet = GetModuleFileNameExA(hProcess, Modules(1), ModuleName, 500)
            sPath = Left$(ModuleName, lRet)
            If Left$(sPath, 4) = "\??\" Then
                sPath = Replace$(sPath, "\??\", vbNullString, 1, 4, 1)
            ElseIf Left$(sPath, 12) = "\SystemRoot\" Then
                sPath = Replace$(sPath, "\SystemRoot", AppVar.Windows, 1, 11, 1)
            End If
            GetProcAddress = sPath
        Else
            GetProcAddress = "SYSTEM"
        End If
    ElseIf pName <> vbNullString Then
        sPath = AppVar.System32 & "\" & pName
        If IsFileExist(sPath) Then
            GetProcAddress = sPath
        Else
            GetProcAddress = "SYSTEM"
        End If
    Else
        GetProcAddress = "SYSTEM"
    End If
    CloseHandle hProcess

End Function

Public Function KillProcAPI(ByVal lPID As Long) As Boolean

Dim lProcess As Long

    lProcess = OpenProcess(PROCESS_TERMINATE, 0&, lPID)
    If lProcess Then
        KillProcAPI = TerminateProcess(lProcess, 0&)
        CloseHandle lProcess
    End If

End Function

Public Function KillProcess(ByVal lPID As Long) As Long

Dim lRet As Boolean

    lRet = KillProcAPI(lPID)
    If lRet Then
        KillProcess = lRet
    Else
        lRet = KillProcWMI(lPID)
        If lRet Then
            KillProcess = 0
        Else
            KillProcess = 999
        End If
    End If

End Function

Public Function KillProcName(ByVal strName As String) As Long

Dim pCount    As Long
Dim hSnapShot As Long
Dim uProcess  As PROCESSENTRY32

    If Len(strName) > 0 Then
        hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0&)
        uProcess.dwSize = Len(uProcess)
        pCount = Process32First(hSnapShot, uProcess)
        Do While pCount
            If LCase$(Left$(uProcess.szExeFile, IIf(InStr(1, uProcess.szExeFile, vbNullChar) > 0, InStr(1, uProcess.szExeFile, vbNullChar) - 1, 0))) = LCase$(strName) Then
                KillProcName = KillProcess(uProcess.th32ProcessID)
            End If
            pCount = Process32Next(hSnapShot, uProcess)
            DoEvents
        Loop
        CloseHandle hSnapShot
    End If

End Function

Public Function KillProcWMI(ByVal lPID As Long) As Long

Dim objWMIService  As Object
Dim colProcessList As Object
Dim objProcess     As Object

    On Error GoTo Hell
    Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
    objWMIService.Security_.Privileges.AddAsString "SeDebugPrivilege", True
    Set colProcessList = objWMIService.ExecQuery("Select * from Win32_Process Where ProcessID = '" & lPID & "'")
    For Each objProcess In colProcessList
        KillProcWMI = objProcess.Terminate()
        DoEvents
    Next objProcess

Exit Function

Hell:
    KillProcWMI = 999

End Function

Public Function RaisePrivileges(ByVal sMode As Boolean) As Boolean

Dim hToken       As Long
Dim SEDebugValue As LUID
Dim hProcess     As Long
Dim lBuffer      As Long
Dim tkp          As TOKEN_PRIVILEGES
Dim tkpNew       As TOKEN_PRIVILEGES
Dim SE_NAME      As String

    On Error GoTo Hell
    RaisePrivileges = False
    hProcess = GetCurrentProcess()
    SE_NAME = IIf(sMode, SE_SHUTDOWN_NAME, SE_DEBUG_NAME)
    If hProcess Then
        If OpenProcessToken(hProcess, TOKEN_ALL_ACCESS, hToken) Then
            If LookupPrivilegeValue("", SE_NAME, SEDebugValue) Then
                With tkp
                    .PrivilegeCount = 1
                    .TheLuid = SEDebugValue
                    .Attributes = SE_PRIVILEGE_ENABLED
                End With
                If AdjustTokenPrivileges(hToken, False, tkp, Len(tkp), tkpNew, lBuffer) Then
                    RaisePrivileges = True
                End If
            End If
        End If
    End If
    CloseHandle hToken
    CloseHandle hProcess
Hell:

End Function

Public Function Thread32_Enum(ByRef Thread() As THREADENTRY32, _
                              ByVal lProcessID As Long) As Long

Dim tProcess  As THREADENTRY32
Dim hSnapShot As Long
Dim lThread   As Long

    Erase Thread()
    hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, lProcessID)
    tProcess.dwSize = Len(tProcess)
    If Thread32First(hSnapShot, tProcess) Then
        ReDim Preserve Thread(lThread) As THREADENTRY32
        Thread(lThread) = tProcess
        Do
            If Thread32Next(hSnapShot, tProcess) Then
                lThread = lThread + 1
                ReDim Preserve Thread(lThread) As THREADENTRY32
                Thread(lThread) = tProcess
            Else
                Exit Do
            End If
            DoEvents
        Loop
        Thread32_Enum = lThread
    Else
        Thread32_Enum = 0
    End If

End Function

Public Function Thread_Resume(ByVal lPID As Long) As Long

Dim i       As Long
Dim hThread As Long

    Thread32_Enum Thread, lPID
    For i = 0 To UBound(Thread)
        If Thread(i).th32OwnerProcessID = lPID Then
            hThread = OpenThread(THREAD_SUSPEND_RESUME, False, Thread(i).th32ThreadID)
            ResumeThread hThread
        End If
        DoEvents
    Next i
    Thread_Resume = hThread

End Function

Public Function Thread_Suspend(lPID As Long) As Long

Dim i       As Long
Dim hThread As Long

    Thread32_Enum Thread, lPID
    For i = 0 To UBound(Thread)
        If Thread(i).th32OwnerProcessID = lPID Then
            hThread = OpenThread(THREAD_SUSPEND_RESUME, False, Thread(i).th32ThreadID)
            SuspendThread hThread
        End If
        DoEvents
    Next i
    Thread_Suspend = hThread

End Function


