Attribute VB_Name = "modSettings"
Option Explicit
Private Reg             As New clsRegistry
Public Const INI_MAIN   As String = "UnLock Them All! Settings"
Private Const INI_REG   As String = "Registry Manager"

Public Sub LoadINI()

Dim i As Integer

    For i = 0 To 37
        formMain.chkRegArr(i).Value = Val(ReadINI(INI_REG, formMain.chkRegArr(i).Caption))
        DoEvents
    Next i

End Sub

Public Sub LoadSettings()

    If IsFileExist(AppVar.AppPath & "\UTASettings.ini") Then
        LoadINI
        GetValue
        MsgBoxEx "Registry Manager settings loaded successfully!", vbInformation, "Load Settings"
    Else
        MsgBoxEx "Cannot find ""UTASettings.ini"" in application's folder!", vbExclamation, "Load Settings"
    End If

End Sub

Private Sub SaveINI()

Dim i        As Integer
Dim sCaption As String

    For i = 0 To 37
        sCaption = formMain.chkRegArr(i).Caption
        If formMain.chkRegArr(i).Value Then
            WriteINI INI_REG, sCaption, "1"
        Else
            WriteINI INI_REG, sCaption
        End If
        DoEvents
    Next i

End Sub

Public Sub SaveLoadState(ByVal State As Single)

    Select Case State
    Case 0
        formMain.menuOptLaunch.Checked = Reg.ValueExists(RUN_HKCU, AppVar.AppTitle)
        formMain.menuOptMin.Checked = ReadINI(INI_MAIN, "Start Minimized") = "True"
        If ReadINI(INI_MAIN, "Minimize to Tray") = "False" Then
            formMain.menuOptKeTray.Checked = False
        End If
        If ReadINI(INI_MAIN, "Show Info on Start") = "False" Then
            formMain.menuOptInfo.Checked = False
        End If
        If ReadINI(INI_MAIN, "Always on Top") = "True" Then
            AlwaysOnTop AppVar.AppHWND, True
            formMain.menuOptOnTop.Checked = True
        End If
        If ReadINI(INI_MAIN, "Transparent Window") = "True" Then
            SetLayered AppVar.AppHWND, True, 200
            formMain.menuOptTrans.Checked = True
        End If
        If ReadINI(INI_MAIN, "Show Confirmation") = "False" Then
            formMain.menuOptConfirm.Checked = False
        End If
    Case 1
        WriteINI INI_MAIN, "Always on Top", formMain.menuOptOnTop.Checked
        WriteINI INI_MAIN, "Start Minimized", formMain.menuOptMin.Checked
        WriteINI INI_MAIN, "Show Info on Start", formMain.menuOptInfo.Checked
        WriteINI INI_MAIN, "Transparent Window", formMain.menuOptTrans.Checked
        WriteINI INI_MAIN, "Minimize to Tray", formMain.menuOptKeTray.Checked
        WriteINI INI_MAIN, "Show Confirmation", formMain.menuOptConfirm.Checked
        WriteINI INI_MAIN, "WindowX", formMain.Left
        WriteINI INI_MAIN, "WindowY", formMain.Top
        WriteINI INI_MAIN, "LastMode", sCurrMode
    End Select

End Sub

Public Sub SaveSettings()

    SaveINI
    MsgBoxEx "All settings saved to ""UTASettings.ini"" in application's folder!", vbInformation, "Save settings"

End Sub

