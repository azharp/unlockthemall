Attribute VB_Name = "modListbox"
Option Explicit
Private Const LB_SETCURSEL               As Long = &H186
Private Const LB_GETCURSEL               As Long = &H188
Private Const LB_SETCARETINDEX           As Long = &H19E
Private Const LB_SETHORIZONTALEXTENT     As Long = &H194
Private Const DT_CALCRECT                As Long = &H400
Private Const SM_CXVSCROLL               As Long = 2
Private Type POINTAPI
    X                                        As Long
    Y                                        As Long
End Type
Private Type Rect
    Left                                     As Long
    Top                                      As Long
    Right                                    As Long
    Bottom                                   As Long
End Type
Private Declare Function DrawText Lib "user32" Alias "DrawTextA" (ByVal hDC As Long, _
                                                                  ByVal lpStr As String, _
                                                                  ByVal nCount As Long, _
                                                                  lpRect As Rect, _
                                                                  ByVal wFormat As Long) As Long
Private Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, _
                                                                        ByVal wMsg As Long, _
                                                                        ByVal wParam As Long, _
                                                                        lParam As Long) As Long
Private Declare Function ClientToScreen Lib "user32" (ByVal hwnd As Long, _
                                                      lpPoint As POINTAPI) As Long
Private Declare Function LBItemFromPt Lib "comctl32.dll" (ByVal hLB As Long, _
                                                          ByVal ptX As Long, _
                                                          ByVal ptY As Long, _
                                                          ByVal bAutoScroll As Long) As Long

Public Sub AddHScroll(ByVal lst As ListBox)

Dim c              As Long
Dim rcText         As Rect
Dim newWidth       As Long
Dim itemWidth      As Long
Dim sysScrollWidth As Long

    sysScrollWidth = GetSystemMetrics(SM_CXVSCROLL)
    For c = 0 To lst.ListCount - 1
        DrawText formMain.hDC, (lst.List(c)), -1&, rcText, DT_CALCRECT
        itemWidth = rcText.Right + sysScrollWidth
        If itemWidth > newWidth Then
            newWidth = itemWidth
        End If
        DoEvents
    Next c
    SendMessage lst.hwnd, LB_SETHORIZONTALEXTENT, newWidth, ByVal 0&

End Sub

Public Function SelectItem(ByVal lst As ListBox, _
                           ByVal X As Single, _
                           ByVal Y As Single, _
                           Optional ByVal Mode As Single = 0) As Long

Dim lIndex As Long
Dim lConst As Long
Dim pNow   As POINTAPI

    If Mode = 0 Then
        lConst = LB_SETCURSEL
    ElseIf Mode = 1 Then
        lConst = LB_SETCARETINDEX
    End If
    pNow.X = X \ Screen.TwipsPerPixelX
    pNow.Y = Y \ Screen.TwipsPerPixelY
    ClientToScreen lst.hwnd, pNow
    lIndex = LBItemFromPt(lst.hwnd, pNow.X, pNow.Y, False)
    If lIndex <> SendMessage(lst.hwnd, LB_GETCURSEL, 0, 0) Then
        SendMessage lst.hwnd, lConst, lIndex, 0
        lst.Refresh
        SelectItem = lIndex
    End If

End Function


