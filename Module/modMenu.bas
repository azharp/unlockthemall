Attribute VB_Name = "modMenu"
Option Explicit
Private Reg          As New clsRegistry
Private bMenuBuilt   As Boolean
Public bMenuMode     As Boolean

Public Sub BuildAllTrayMenu()

    bMenuMode = True
    Erase mProcNow
    Erase mStrInfo
    Erase mVisNow
    Erase mInvNow
    GetAllProcess
    GetAllStartup 2
    GetAllWindows
    BuildProcMenu
    BuildStrMenu
    BuildWndMenu
    If bMenuBuilt Then
        CheckRegMenu
    Else
        BuildRegMenu
        bMenuBuilt = True
    End If
    formMain.tmrRefresh.Enabled = True

End Sub

Private Sub BuildProcMenu()

Dim i      As Long
Dim mCount As Long

    On Error Resume Next
    With formMain
        EmptyMenu .menuTrayProcArr
        For i = 0 To UBound(mProcNow)
            mCount = .menuTrayProcArr.UBound + 1
            Load .menuTrayProcArr(mCount)
            .menuTrayProcArr(mCount).Caption = mProcNow(i).sName
            .menuTrayProcArr(mCount).Tag = mProcNow(i).lPID
            .menuTrayProcArr(mCount).Enabled = True
            .menuTrayProcArr(mCount).Visible = True
            DoEvents
        Next i
        If .menuTrayProcArr.Count > 1 Then
            .menuTrayProcArr(0).Visible = False
        End If
        .menuTrayProc.Caption = "Process Manager" & vbTab & "[" & SysVar.Process & "]"
    End With
    On Error GoTo 0

End Sub

Private Sub BuildRegMenu()

Dim i      As Integer
Dim mCount As Long

    On Error Resume Next
    With formMain
        EmptyMenu .menuTrayRegArr
        For i = 0 To 37
            mCount = .menuTrayRegArr.UBound + 1
            Load .menuTrayRegArr(mCount)
            .menuTrayRegArr(mCount).Caption = RegCaptionArr(i)
            .menuTrayRegArr(mCount).Tag = RegNameArr(i)
            .menuTrayRegArr(mCount).Enabled = True
            DoEvents
        Next i
        CheckRegMenu
        If .menuTrayRegArr.Count > 1 Then
            .menuTrayRegArr(0).Visible = False
        End If
    End With
    On Error GoTo 0

End Sub

Private Sub BuildStrMenu()


Dim i      As Long
Dim mCount As Long

    On Error Resume Next
    With formMain
        EmptyMenu .menuTrayStrAllArr
        For i = 0 To UBound(mStrInfo)
            If mStrInfo(i).sLoc = "RegAll" Then
                mCount = .menuTrayStrAllArr.UBound + 1
                Load .menuTrayStrAllArr(mCount)
                .menuTrayStrAllArr(mCount).Caption = mStrInfo(i).sName
                .menuTrayStrAllArr(mCount).Tag = mStrInfo(i).sPath
                .menuTrayStrAllArr(mCount).Enabled = True
            End If
            DoEvents
        Next i
        If .menuTrayStrAllArr.Count > 1 Then
            .menuTrayStrAllArr(0).Visible = False
        End If
        .menuTrayStrAll.Caption = "[Registry] All User" & vbTab & "[" & .menuTrayStrAllArr.Count - 1 & "]"
        EmptyMenu .menuTrayStrCurArr
        For i = 0 To UBound(mStrInfo)
            If mStrInfo(i).sLoc = "RegCur" Then
                mCount = .menuTrayStrCurArr.UBound + 1
                Load .menuTrayStrCurArr(mCount)
                .menuTrayStrCurArr(mCount).Caption = mStrInfo(i).sName
                .menuTrayStrCurArr(mCount).Tag = mStrInfo(i).sPath
                .menuTrayStrCurArr(mCount).Enabled = True
            End If
            DoEvents
        Next i
        If .menuTrayStrCurArr.Count > 1 Then
            .menuTrayStrCurArr(0).Visible = False
        End If
        .menuTrayStrCur.Caption = "[Registry] Current User" & vbTab & "[" & .menuTrayStrCurArr.Count - 1 & "]"
        EmptyMenu .menuTrayStrAllFArr
        For i = 0 To UBound(mStrInfo)
            If mStrInfo(i).sLoc = "FolAll" Then
                mCount = .menuTrayStrAllFArr.UBound + 1
                Load .menuTrayStrAllFArr(mCount)
                .menuTrayStrAllFArr(mCount).Caption = mStrInfo(i).sName
                .menuTrayStrAllFArr(mCount).Tag = mStrInfo(i).sPath
                .menuTrayStrAllFArr(mCount).Enabled = True
            End If
            DoEvents
        Next i
        If .menuTrayStrAllFArr.Count > 1 Then
            .menuTrayStrAllFArr(0).Visible = False
        End If
        .menuTrayStrAllF.Caption = "[Folder] All User" & vbTab & "[" & .menuTrayStrAllFArr.Count - 1 & "]"
        EmptyMenu .menuTrayStrCurFArr
        For i = 0 To UBound(mStrInfo)
            If mStrInfo(i).sLoc = "FolCur" Then
                mCount = .menuTrayStrCurFArr.UBound + 1
                Load .menuTrayStrCurFArr(mCount)
                .menuTrayStrCurFArr(mCount).Caption = mStrInfo(i).sName
                .menuTrayStrCurFArr(mCount).Tag = mStrInfo(i).sPath
                .menuTrayStrCurFArr(mCount).Enabled = True
            End If
            DoEvents
        Next i
        If .menuTrayStrCurFArr.Count > 1 Then
            .menuTrayStrCurFArr(0).Visible = False
        End If
        .menuTrayStrCurF.Caption = "[Folder] Current User" & vbTab & "[" & .menuTrayStrCurFArr.Count - 1 & "]"
        .menuTrayStr.Caption = "Startup Manager" & vbTab & "[" & SysVar.Startup & "]"
    End With
    On Error GoTo 0

End Sub

Private Sub BuildWndMenu()

Dim i      As Long
Dim mCount As Long

    On Error Resume Next
    With formMain
        EmptyMenu .menuTrayWndVisArr
        For i = 0 To UBound(mVisNow)
            mCount = .menuTrayWndVisArr.UBound + 1
            Load .menuTrayWndVisArr(mCount)
            .menuTrayWndVisArr(mCount).Caption = mVisNow(i).sCaption
            .menuTrayWndVisArr(mCount).Tag = mVisNow(i).lHWND
            .menuTrayWndVisArr(mCount).Enabled = True
            .menuTrayWndVisArr(mCount).Visible = True
            DoEvents
        Next i
        If .menuTrayWndVisArr.Count > 1 Then
            .menuTrayWndVisArr(0).Visible = False
        End If
        .menuTrayWndVis.Caption = "Visible Windows" & vbTab & "[" & .menuTrayWndVisArr.Count - 1 & "]"
        EmptyMenu .menuTrayWndInvArr
        For i = 0 To UBound(mInvNow)
            mCount = .menuTrayWndInvArr.UBound + 1
            Load .menuTrayWndInvArr(mCount)
            .menuTrayWndInvArr(mCount).Caption = mInvNow(i).sCaption
            .menuTrayWndInvArr(mCount).Tag = mInvNow(i).lHWND
            .menuTrayWndInvArr(mCount).Enabled = True
            .menuTrayWndInvArr(mCount).Visible = True
            DoEvents
        Next i
        If .menuTrayWndInvArr.Count > 1 Then
            .menuTrayWndInvArr(0).Visible = False
        End If
        .menuTrayWndInv.Caption = "Invisible Windows" & vbTab & "[" & .menuTrayWndInvArr.Count - 1 & "]"
        .menuTrayWnd.Caption = "Window Manager" & vbTab & "[" & SysVar.Window + SysVar.WindowInv & "]"
    End With
    On Error GoTo 0

End Sub

Private Sub CheckRegMenu()


Dim i    As Integer
Dim sTag As String

    SysVar.Registry = 0
    For i = 1 To 38
        sTag = RegNameArr(i - 1)
        Select Case sTag
        Case "DisableRegistryTools", "DisableTaskMgr", "NoDispCpl"
            Select Case Reg.ReadDWORD(KEY_SYSTEM, sTag, 0)
            Case 1
                formMain.menuTrayRegArr(i).Checked = 1
                SysVar.Registry = SysVar.Registry + 1
            Case 0
                formMain.menuTrayRegArr(i).Checked = 0
            End Select
        Case "NoEntireNetwork", "NoWorkgroupContents", "NoNetSetup"
            Select Case Reg.ReadDWORD(KEY_NETWORK, sTag, 0)
            Case 1
                formMain.menuTrayRegArr(i).Checked = 1
                SysVar.Registry = SysVar.Registry + 1
            Case 0
                formMain.menuTrayRegArr(i).Checked = 0
            End Select
        Case "LockTaskbar", "NoDrives", "NoViewOnDrive", "NoSimpleStartMenu", "NoSaveSettings"
            Select Case Reg.ReadDWORD(KEY_EXPLORER, sTag, 0)
            Case 1, 4
                formMain.menuTrayRegArr(i).Checked = 1
                SysVar.Registry = SysVar.Registry + 1
            Case 0
                formMain.menuTrayRegArr(i).Checked = 0
            End Select
        Case "DisableCMD"
            Select Case Reg.ReadDWORD("HKCU\Software\Policies\Microsoft\Windows\System", "DisableCMD", 0)
            Case 1
                formMain.menuTrayRegArr(i).Checked = 1
                SysVar.Registry = SysVar.Registry + 1
            Case 0
                formMain.menuTrayRegArr(i).Checked = 0
            End Select
        Case "NoMyComputer"
            Select Case Reg.ReadDWORD("HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\NonEnum", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 0)
            Case 1
                formMain.menuTrayRegArr(i).Checked = 1
                SysVar.Registry = SysVar.Registry + 1
            Case 0
                formMain.menuTrayRegArr(i).Checked = 0
            End Select
        Case Else
            Select Case Reg.ReadDWORD(KEY_EXPLORER, sTag, 0)
            Case 1, 4, 127
                formMain.menuTrayRegArr(i).Checked = 1
                SysVar.Registry = SysVar.Registry + 1
            Case 0
                formMain.menuTrayRegArr(i).Checked = 0
            End Select
        End Select
        DoEvents
    Next i
    formMain.menuTrayReg.Caption = "Registry Manager" & vbTab & "[" & SysVar.Registry & "]"

End Sub

Private Sub EmptyMenu(ByVal ArrMenu As Object)

Dim i As Long

    On Error Resume Next
    ArrMenu(0).Visible = True
    For i = 1 To ArrMenu.UBound
        Unload ArrMenu(i)
    Next i
    On Error GoTo 0

End Sub

Private Function HideMenu(ByVal oMenu As Object, _
                          ByVal sData As Long) As Boolean

Dim i As Long

    For i = 1 To oMenu.Count - 1
        If oMenu(i).Tag = sData Then
            oMenu(i).Visible = False
            HideMenu = True
            Exit Function
        End If
        DoEvents
    Next i
    HideMenu = False

End Function

Public Sub IncrementMenu()


Dim i      As Long
Dim mCount As Long

    On Error Resume Next
    CopyProcType mProcNow, mProcOld
    CopyWndType mVisNow, mVisOld
    CopyWndType mInvNow, mInvOld
    GetAllProcess
    GetAllWindows
    With formMain
        For i = 0 To UBound(mProcNow)
            If Not FindProcData(mProcOld, mProcNow(i).lPID) Then
                mCount = .menuTrayProcArr.UBound + 1
                Load .menuTrayProcArr(mCount)
                .menuTrayProcArr(mCount).Caption = mProcNow(i).sName
                .menuTrayProcArr(mCount).Tag = mProcNow(i).lPID
                .menuTrayProcArr(mCount).Enabled = True
                .menuTrayProcArr(mCount).Visible = True
            End If
            DoEvents
        Next i
        For i = 0 To UBound(mProcOld)
            If Not FindProcData(mProcNow, mProcOld(i).lPID) Then
                HideMenu .menuTrayProcArr, mProcOld(i).lPID
            End If
            DoEvents
        Next i
        .menuTrayProc.Caption = "Process Manager" & vbTab & "[" & SysVar.Process & "]"
        For i = 0 To UBound(mVisNow)
            If Not FindWndData(mVisOld, mVisNow(i).lHWND) Then
                mCount = .menuTrayWndVisArr.UBound + 1
                Load .menuTrayWndVisArr(mCount)
                .menuTrayWndVisArr(mCount).Caption = mVisNow(i).sCaption
                .menuTrayWndVisArr(mCount).Tag = mVisNow(i).lHWND
                .menuTrayWndVisArr(mCount).Enabled = True
                .menuTrayWndVisArr(mCount).Visible = True
            End If
            DoEvents
        Next i
        For i = 0 To UBound(mVisOld)
            If Not FindWndData(mVisNow, mVisOld(i).lHWND) Then
                HideMenu .menuTrayWndVisArr, mVisOld(i).lHWND
            End If
            DoEvents
        Next i
        .menuTrayWndVis.Caption = "Visible Windows" & vbTab & "[" & .menuTrayWndVisArr.Count - 1 & "]"
        For i = 0 To UBound(mInvNow)
            If Not FindWndData(mInvOld, mInvNow(i).lHWND) Then
                mCount = .menuTrayWndInvArr.UBound + 1
                Load .menuTrayWndInvArr(mCount)
                .menuTrayWndInvArr(mCount).Caption = mInvNow(i).sCaption
                .menuTrayWndInvArr(mCount).Tag = mInvNow(i).lHWND
                .menuTrayWndInvArr(mCount).Enabled = True
                .menuTrayWndInvArr(mCount).Visible = True
            End If
            DoEvents
        Next i
        For i = 0 To UBound(mInvOld)
            If Not FindWndData(mInvNow, mInvOld(i).lHWND) Then
                HideMenu .menuTrayWndVisArr, mVisOld(i).lHWND
            End If
            DoEvents
        Next i
        .menuTrayWndInv.Caption = "Invisible Windows" & vbTab & "[" & .menuTrayWndInvArr.Count - 1 & "]"
        .menuTrayWnd.Caption = "Window Manager" & vbTab & "[" & SysVar.Window + SysVar.WindowInv & "]"
    End With
    On Error GoTo 0

End Sub


